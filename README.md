# CMS - Contract Management System
### Installation

1. Clone this repository: `git clone <repository-url>`

2. Navigate to the cloned repository folder using `cd` command.

3. Install dependencies using npm: `npm install`

### Development

To run the development server: `npm run dev`

This will start the Next.js development server, and you can access the application at [http://localhost:3000](http://localhost:3000/).

### Building

To build the project for production: `npm run build`

This will generate a `build` folder with optimized production-ready code.

### Starting the Production Server

After building, you can start the production server: `npm start`

### Code Linting

Linting is set up using ESLint and Prettier. To lint the code: `npm run lint`

### Code Formatting

To format the code using Prettier: `npm run format`

To automatically fix formatting issues: `npm run format:fix`

## Dependencies

* **Next.js**: The main framework for building React applications with server-side rendering.
* **React**: A JavaScript library for building user interfaces.
* **Tailwind CSS**: A utility-first CSS framework.
* **ESLint**: A tool for identifying and fixing problems in JavaScript code.
* **Prettier**: An opinionated code formatter.
* **TypeScript**: A typed superset of JavaScript.

## Scripts

* `npm start`: Start the production server.
* `npm run dev`: Start the development server.
* `npm run build`: Build the project for production.
* `npm run lint`: Run ESLint for code linting.
* `npm run format`: Check code formatting using Prettier.
* `npm run format:fix`: Automatically fix code formatting issues.
