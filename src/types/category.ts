export interface ICategory {
  id: string;
  name: string;
  createdBy: string;
  createdAt: Date;
}
