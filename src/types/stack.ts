export interface IStack {
  id: string;
  name: string;
  stackItems: string[];
  createdBy: string;
  createdAt: Date;
}
