export interface IContract {
  id: string;
  companyName: string;
  stack: string;
  startDate: Date;
  endDate: Date;
  locations: number;
  mrr: number;
  totalContractValue: number;
  computerUsers: number;
  emailOnlyUsers: number;
  workstations: number;
  servers: number;
  createdBy: string;
  createdAt: Date;
}
