export interface IAlert {
  details: string;
  type: string;
  date: Date;
  contractID: string;
}
