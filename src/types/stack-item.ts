export interface IStackItem {
  id: string;
  name: string;
  category: string;
  description: string;
  quantity: number;
  unitCost: number;
  createdBy: string;
  createdAt: Date;
}
