import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '..';

// Define a type for the slice state
interface CounterState {
  user: { email: string; uid: string; displayName: string; photoUrl: string };
}

// Define the initial state using that type
const initialState: CounterState = {
  user: { email: '', uid: '', displayName: '', photoUrl: '' },
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    signIn: (state, action) => {
      state.user = action.payload;
    },
    logout: (state) => {
      state.user = { email: '', uid: '', displayName: '', photoUrl: '' };
    },
  },
});

export const { signIn, logout } = userSlice.actions;

// selectors
export const selectUser = (state: RootState) => state?.user?.user;

export const userReducer = userSlice.reducer;
