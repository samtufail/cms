import dynamic from 'next/dynamic';
import { Chart as ChartImport } from './chart.component';
import { Layout as LayoutImport } from './layout.component';

export { Discovery } from './discovery-info.component';

export { Input } from './input.component';
export { Button } from './button.component';
export { Header } from './header.component';
export { DButton } from './dbButton.component';
export { ContractCard } from './contract-card.component';
export { Alert } from './alert.component';
export { AlertDetail } from './alert-detail.component';
export { ChartDetails } from './chart-details.component';
export { PayChartDetail } from './paychart-detail.component';
export { Paying } from './paying.component';
// export { Profit } from './profit.component';
export { StackCard } from './stack-card.component';
export { Service } from './service.component';
export { CustomDatePicker as DatePicker } from './date-picker.component';
export { CustomTimePicker as TimePicker } from './time-picker.component';
export { CustomSelect } from './custom-select.component';
export { StackModal } from './stack-modal.component';
export { ServiceModal } from './service-modal.component';
export { StackDetail } from './stack-detail.component';
export { MatrixCard } from './matrix-card.component';
export { LinesChart } from './line-chart.component';
export { ContractData } from './contract-data.component';
export {CustomMultiSelect} from './custom-multi-select.component';

export const Chart = dynamic(() => import('./chart.component').then((module) => module.Chart) as any, {
  ssr: false,
}) as typeof ChartImport;

export const Layout = dynamic(() => import('./layout.component').then((module) => module.Layout) as any, {
  ssr: false,
}) as typeof LayoutImport;
