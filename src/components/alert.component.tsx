import { ArrowRightIcon } from "@heroicons/react/24/outline";

interface AlertProps {
  details?: string;
  icon?: string;
  type?: string;
}

export const Alert = ({
  details = "XXX Company is expiring soon.",
  icon = "/icon/info.svg",
  type,
}: AlertProps) => {
  let alertClass = "text-[#000000]";
  if (type === "info") {
    alertClass = "text-blue-800 bg-[#EFF6FF]"; // Change the color for expiring alerts
  } else if (type === "success") {
    alertClass = "text-green-700 bg-[#ECFDF5]"; // Change the color for added alerts
  } else if (type === "error") {
    alertClass = "text-red-700 bg-[#FEF2F2]"; // Change the color for limited alerts
  }

  return (
    <div
      className={`flex ${alertClass} justify-between p-[16px] rounded-[6px]`}
    >
      <img src={icon} alt={icon} className="pr-[8px]" />
      {details}
      <button className="flex gap-[3px] items-center ml-auto px-2">
        Details <ArrowRightIcon className="block h-4 w-4" aria-hidden="true" />
      </button>
    </div>
  );
};
