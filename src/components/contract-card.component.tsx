import { getById } from '@/api/stack';
import { formatCurrency } from '@/utils';
import { ArrowTrendingDownIcon, CloudIcon } from '@heroicons/react/24/outline';
import dayjs from 'dayjs';

import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

interface ContractCardProps {
  companyName: string;
  stack: string;
  mrr: number;
  totalContractValue: number;
  computerUsers: number;
  emailOnlyUsers: number;
  workStations: number;
  servers: number;
  endDate?: string;
  isDraft?: boolean;
}

export const ContractCard = ({
  companyName = 'XYZ Company',
  stack = '5hEnlCpGuj5O0XU01rqB',
  mrr = 7345,
  totalContractValue = 25412,
  computerUsers = 23,
  emailOnlyUsers = 5,
  workStations = 23,
  servers = 3,
  endDate = '12/04/2025',
  isDraft = false,
}: ContractCardProps) => {
  const [loading, setLoading] = useState(false);
  const [stackDetails, setStackDetails] = useState<any>({});

  const router = useRouter();

  useEffect(() => {
    (async () => {
      setLoading(true);
      if (stack) {
        const stackDetails = await getById(stack);
        setStackDetails(stackDetails);
      }
      setLoading(false);
    })();
  }, [stack]);

  return (
    <div className="flex flex-col items-start text-[#000000] shadow border border-[#CECECE] rounded-lg  p-[12px]">
      <div className="flex items-center gap-[12px] text-[26px] font-[700]">
        <CloudIcon className="block h-6 w-6" aria-hidden="true" />
        <h3>{companyName}</h3>
      </div>
      <h3 className="text-[16px] font-[700]">{stackDetails?.name || 'No Stack'}</h3>
      <div className="mt-[12px] text-[14px] font-[700]">
        {isDraft ? (
          <></>
        ) : (
          <div className='flex items-start flex-col'>
            <p className='text-left'>MRR: {typeof mrr !== 'string' ? formatCurrency(mrr) : `${mrr}`}</p>
            <p className='text-left'>TCV: {typeof totalContractValue !== 'string' ? formatCurrency(totalContractValue) : `${totalContractValue}`}</p>
          </div>
        )}
      </div>
      <div className="flex flex-col items-start mt-[12px] text-[14px]">
        <p>
          Computer Users: {computerUsers}
          {/*| <span className="text-[#2CA01C]"> +2</span> */}
        </p>
        <p>
          Email Only Users: {emailOnlyUsers}
          {/* |<span className="text-[#A01C1C]"> -7</span> */}
        </p>
        <p>Workstations: {workStations}</p>
        <p>Servers: {servers}</p>
      </div>
      <div className="flex items-center gap-[12px] mt-[2rem] mb-[1rem] text-[16px] font-[700]">
        <ArrowTrendingDownIcon className="block h-6 w-6" aria-hidden="true" />
        <h3>Expiring on: {dayjs(endDate).format('MM-DD-YYYY')}</h3>
      </div>
    </div>
  );
};
