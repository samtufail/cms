import Head from 'next/head';
import { ReactNode } from 'react';
import dynamic from 'next/dynamic';
import BarLoader from 'react-spinners/BarLoader';
import { selectUser, useAppSelector } from '@/redux';

export const Navbar = dynamic(() => import('./navbar.component').then((module) => module.Navbar) as any, {
  ssr: false,
});

export const Layout = ({ children, loading = false }: { children: ReactNode; loading?: boolean }) => {
  const user = useAppSelector(selectUser);
  return (
    <>
      <Head>
        <title>Dashboard</title>
      </Head>
      {!user?.uid || loading ? (
        <div className="w-screen h-screen flex items-center justify-center">
          <BarLoader loading={true} aria-label="Loading Spinner" data-testid="loader" color="#1e3b7d" />
        </div>
      ) : (
        <div className="bg-[#F3F4F6] min-h-screen">
          <Navbar />
          <div className="bg-[#071638] min-h-[18rem]" />
          <div className="-mt-[17rem]">{children}</div>
        </div>
      )}
    </>
  );
};
