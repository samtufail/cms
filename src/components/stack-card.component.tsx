import { DButton } from '@/components';
import { useRouter } from 'next/router';
import { Popconfirm } from 'antd';
import { del } from '@/api/stack';
import { useEffect, useState } from 'react';

export const StackCard = ({ name = 'Managed Services Package', stackItems = [], id = '', contracts = [] }) => {
  const [avail, setAvail] = useState(false);
  const router = useRouter();

  useEffect(() => {
    if (contracts?.filter((c: any) => c?.stack === id)?.length) {
      setAvail(true);
    }
  }, [contracts, id]);

  return (
    <div className="text-[#000000] shadow border border-[#CECECE] rounded-lg p-[12px] m-[10px] relative">
      <h3 className="text-[16px] font-[700]">{name}</h3>
      <div className="mt-[12px] text-[14px]">
        <p>Items in this template: {stackItems?.length}</p>
      </div>
      <div className="flex items-center gap-[12px] mt-[2rem] text-[16px] font-[700]">
        <DButton
          type="button"
          onClick={() => {
            router.push(`/stack/${id}`);
          }}
          className="flex items-center justify-center font-bold text-[#000] underline"
        >
          <span>View Details</span>
        </DButton>
      </div>
      {!avail ? (
        <Popconfirm
          title="Are you sure you want to delete this item? This might be used in a contract and cannot be undone."
          onConfirm={async () => {
            await del(id);
            router.reload();
          }}
          okText="Yes"
          cancelText="No"
        >
          <DButton type="button" className="absolute top-0 right-0">
            <img className="w-4 h-4" src="/icon/bin.svg" alt="bin" />
          </DButton>
        </Popconfirm>
      ) : (
        <></>
      )}
    </div>
  );
};
