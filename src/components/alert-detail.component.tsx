import { ArrowRightIcon } from '@heroicons/react/24/outline';

interface AlertProps {
  details?: string;
  icon?: string;
  date?: string;
  time?: string;
  type?: string;
}

export const AlertDetail = ({
  details = 'XXX Company is expiring soon.',
  icon = '/icon/info.svg',
  date = 'June 26, 2023',
  time = '11:30 AM',
  type,
}: AlertProps) => {
  let alertClass = 'text-[#000000]';
  if (type === 'info') {
    alertClass = 'text-blue-800 bg-[#EFF6FF]'; // Change the color for expiring alerts
  } else if (type === 'success') {
    alertClass = 'text-green-700 bg-[#ECFDF5]'; // Change the color for added alerts
  } else if (type === 'error') {
    alertClass = 'text-red-700 bg-[#FEF2F2]'; // Change the color for limited alerts
  }

  return (
    <div className={`flex md:justify-between p-[16px] rounded-[6px] md:flex-row flex-col gap-3`}>
      <div className={`${alertClass}`}>
        <div className="flex ">
          <img src={icon} alt={icon} className="pr-[8px]" />
          <h3 className="md:text-[16px] text-[13px] font-[700]">{details}</h3>
        </div>
        <p className="ml-[29px] md:text-[14px] text-[12px] font-[400]">
          {date} | {time}
        </p>
      </div>
      <button className="flex gap-[3px] items-center px-2 md:text-[16px] text-[13px]">
        Details <ArrowRightIcon className="block h-4 w-4 " aria-hidden="true" />
      </button>
    </div>
  );
};
