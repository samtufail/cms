interface AlertProps {
  sr?: string;
  company?: string;
  mrr?: string;
  cv?: string;
  status?: string;
}

export const Paying = ({
  sr = "1",
  company = "Demo Company",
  mrr = "$12,423",
  cv = "$12,423",
  status = "Active",
}: AlertProps) => {
  
  let badgeClass = "text-[#000000] rounded-full px-2 py-1";
  if (status === "Active") {
    badgeClass += " bg-green-200 text-green-600";
  } else if (status === "Inactive") {
    badgeClass += " bg-red-200 text-red-600";
  }

  return (
    <tbody>
      <tr className="text-14 font-500">
        <th scope="row" className="px-6 py-4 whitespace-nowrap">
          {sr}
        </th>
        <td className="px-6 py-4">{company}</td>
        <td className="px-6 py-4">{mrr}</td>
        <td className="px-6 py-4">{cv}</td>
        <td className="px-6 py-4">
          <div className={`inline-block ${badgeClass}`}>{status}</div>
        </td>
      </tr>
    </tbody>
  );
};
