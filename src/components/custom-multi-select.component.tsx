import { Field } from 'formik';
import { Select } from 'antd';

export const CustomMultiSelect = ({ name, label, onChange, placeholder, customErrorLabel, children }: any) => {
  return (
    <Field name={name}>
      {({
        field, // { name, value, onChange, onBlur }
        form: { touched, errors, setFieldValue }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
        meta,
      }: any) => {
        const isError = meta.touched && meta.error;
        return (
          <div className="flex flex-col">
            <label htmlFor={name} className="text-[14px] mb-[4px]">
              {label}
            </label>
            <Select
              onChange={(value: any) => {
                setFieldValue(name, value); // Set the selected value directly
                if (onChange) onChange(value);
              }}
              value={field?.value}
              mode="multiple"
              // style={{ height: '56px' }}
              className={`text-[#071538] placeholder:text-[#6B7280]  rounded-[6px] ${isError ? 'border-red-500 border' : ''}`}
              placeholder={placeholder}
            >
              {children}
            </Select>
            {!customErrorLabel?.text && meta.touched && meta.error && <div className="text-red-500">{meta.error}</div>}
            {customErrorLabel?.text && meta.touched && meta.error && (
              <div className="flex w-full justify-between">
                <div className="text-red-500">{meta.error}</div>
                <button type="button" onClick={() => customErrorLabel?.action()}>
                  {customErrorLabel?.text}
                </button>
              </div>
            )}
          </div>
        );
      }}
    </Field>
  );
};
