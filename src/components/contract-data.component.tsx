export const ContractData = ({
  changeDate = "11/15/2023",
  changetype = 200,
  changePrice = "$12,423",
  mrc = "$12,423",
}: any) => {
  return (
    <tbody>
      <tr className="text-14 font-500 border-b border-gray-300">
        <td className="px-6 py-4">{changeDate}</td>
        <td className="px-6 py-4">{changetype}</td>
        <td className="px-6 py-4">{changePrice}</td>
        <td className="px-6 py-4">{mrc}</td>
      </tr>
    </tbody>
  );
};
