import React, { useState } from 'react';

export const Discovery = ({ name = 'locations', label = '', value = 0, setFieldValue }: any) => {
  // Add leading zero for numbers less than 10
  const formatNumber = (num: any) => {
    if (num < 10) {
      return `0${num}`;
    }
    return num;
  };

  const incrementQuantity = () => {
    setFieldValue(name, value + 1);
  };

  const decrementQuantity = () => {
    if (value > 0) {
      setFieldValue(name, value - 1);
    }
  };

  return (
    <div className="flex justify-between items-center bg-[#FFF] p-[18px] border-b border-gray-300">
      <h6 className="text-[14px] font-semibold">{label}</h6>
      <div className="flex items-center">
        <button onClick={decrementQuantity}>-</button>
        <h6 className="text-[14px] font-semibold mx-2 w-[50px] border rounded  text-center">{formatNumber(value)}</h6>
        <button onClick={incrementQuantity}>+</button>
      </div>
    </div>
  );
};
