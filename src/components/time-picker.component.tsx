import { Field } from "formik";
import type { TimePickerProps } from "antd";
import { TimePicker } from "antd";

export const CustomTimePicker = ({
  name,
  type,
  label,
  placeholder,
  customErrorLabel,
}: any) => {
  return (
    <Field name={name}>
      {({
        field, // { name, value, onChange, onBlur }
        form: { touched, errors, setFieldValue }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
        meta,
      }: any) => {
        const isError = meta.touched && meta.error;
        const onChange: TimePickerProps["onChange"] = (time, timeString) => {
          setFieldValue(name, timeString);
        };

        return (
          <div className="">
            <TimePicker
              onChange={onChange}
              className="h-[56px] text-[#071538] placeholder:text-[#6B7280] px-[13px] py-[9px] rounded-[6px]"
              style={isError ? { border: "1px solid red" } : {}}
              placeholder={placeholder}
            />
            {!customErrorLabel?.text && meta.touched && meta.error && (
              <div className="text-red-500">{meta.error}</div>
            )}
            {customErrorLabel?.text && meta.touched && meta.error && (
              <div className="flex w-full justify-between">
                <div className="text-red-500">{meta.error}</div>
                <button
                  type="button"
                  onClick={() => customErrorLabel?.action()}
                >
                  {customErrorLabel?.text}
                </button>
              </div>
            )}
          </div>
        );
      }}
    </Field>
  );
};
