import { useEffect, useState } from 'react';
import { DButton } from './dbButton.component';
import { add, update } from '@/api/stack';
import { useRouter } from 'next/router';

export const StackModal = ({ stack, cancelButton, isAdd }: any) => {

  const [stackData, setStackData] = useState('');

  // Only for Update
  useEffect(() => {
    if (stack?.name) {
      setStackData(stack?.name);
    }
  }, [stack]);

  const router = useRouter();
  const handleSave = async () => {
    // Only for Add
    if (isAdd) {
      await add({
        name: stackData,
      });
      window.location.reload();
    }

    // Only for Update
    else {
      const id = router.query?.id as string;
      await update(id, { ...stack, name: stackData });
      window.location.reload();
    }
  };

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50">
      <div className="fixed inset-0 bg-black opacity-50"></div>
      <div className="z-10 bg-[#fff] w-[430px] rounded-lg py-[15px] px-[12px]">
        <h2 className="text-2xl font-semibold mb-4 mt-[12px]">{isAdd ? 'Add' : 'Edit'} Stack Name</h2>
        <div className="flex flex-col gap-[6px]">
          <label>Stack Name</label>
          <input
            type="text"
            title="Service Name"
            className="border border-gray-300 p-2 rounded-md w-full"
            value={stackData}
            onChange={(e) => setStackData(e.target.value)}
          />
        </div>
        <div className="mt-4 flex justify-end gap-[4px]">
          <DButton onClick={cancelButton} className="bg-[#909090] text-[#fff] px-[16px]">
            Cancel
          </DButton>
          <DButton onClick={handleSave} className="bg-[#4F46E5] text-[#fff] w-[134px] px-[12px]">
            Save Changes
          </DButton>
        </div>
      </div>
    </div>
  );
};
