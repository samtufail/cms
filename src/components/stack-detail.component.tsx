import { useEffect, useState } from 'react';
import { DButton } from './dbButton.component';
import { EditStackModal } from './edit-stack.component';
import { Popconfirm, message } from 'antd';

interface StackProps {
  id: string;
  name: string;
  description: string;
  quantity: number;
  unitCost: number;
  category: string;
  onDelete: (id: string) => void;
  onUpdate: (editedData: any) => void;
}

export const StackDetail: React.FC<StackProps> = ({
  id = 'abc',
  name = 'Estimated Labor of 0.5 Hours per Computer User',
  description = 'Phishing Training',
  unitCost = 17.5,
  quantity = 23,
  category = '',
  onDelete,
  onUpdate,
}) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [editedData, setEditedData] = useState<any>({});

  useEffect(() => {
    setEditedData({ id, name, description, quantity, unitCost, category });
  }, [id, name, description, quantity, unitCost, category]);

  const openEditModal = () => {
    setIsModalOpen(true);
  };

  const closeEditModal = () => {
    setIsModalOpen(false);
  };

  const saveChanges = (updatedData: any) => {
    setEditedData(updatedData);
    onUpdate(updatedData);
    closeEditModal();
  };

  const handleDelete = () => {
    message.success('deleted successfully');
    onDelete(id);
  };

  return (
    <tbody>
      <tr className="text-[14px] font-[500] border-b border-gray-300">
        <td className="px-6 py-4">{editedData?.name}</td>
        <td className="px-6 py-4">{editedData?.description}</td>
        <td className="px-6 py-4">{editedData?.quantity}</td>
        <td className="px-6 py-4">{editedData?.unitCost}</td>
        <td className="px-6 py-4">{Number(editedData?.quantity) * Number(editedData?.unitCost)}</td>
        <td className="flex px-2 py-4">
          <DButton type="button" onClick={openEditModal}>
            <img className="w-4 h-4" src="/icon/pencil.svg" alt="pencil" />
          </DButton>
          <Popconfirm title="Are you sure you want to delete this row?" onConfirm={handleDelete} okText="Yes" cancelText="No">
            <DButton type="button">
              <img className="w-4 h-4" src="/icon/bin.svg" alt="bin" />
            </DButton>
          </Popconfirm>
        </td>
      </tr>

      {/* Edit Modal */}
      {isModalOpen && <EditStackModal onClose={closeEditModal} onSave={saveChanges} initialData={editedData} />}
    </tbody>
  );
};
