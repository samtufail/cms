import { Field } from 'formik';

export const Input = ({ alwaysShowErrorLabel, name, type, label, placeholder, readOnly, customErrorLabel, className, style, customOnChange }: any) => {
  return (
    <Field name={name}>
      {({
        field, // { name, value, onChange, onBlur }
        form: { touched, errors, setFieldValue }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
        meta,
      }: any) => {
        const isError = meta.touched && meta.error;
        return (
          <div className="flex flex-col">
            <label htmlFor={name} className="text-[14px] mb-[4px]">
              {label}
            </label>
            <input
              id={name}
              name={name}
              type={type}
              style={style ? style : {}}
              placeholder={placeholder}
              readOnly={readOnly}
              step=".01"
              onChange={(e: any) => {
                if (type === 'number') {
                  setFieldValue(name, Number(e?.target?.value)); // Set the selected value directly
                  if (customOnChange) {
                    customOnChange(Number(e?.target?.value));
                  }
                } else {
                  setFieldValue(name, e?.target?.value);
                  if (customOnChange) {
                    customOnChange(e?.target?.value);
                  }
                }
              }}
              value={field?.value}
              className={`h-[56px] text-[#071538] placeholder:text-[#cfcece] px-[13px] py-[9px] rounded-[6px] border border-[#d9d9d9] hover:border-[#1677ff] cursor-pointer focus:outline-none focus:ring-1 focus:ring-[#1677ff] ${isError ? 'border-red-600 border' : ''
                } ${className}`}
            // {...field}
            />
            {!customErrorLabel?.text && meta.touched && meta.error ? <div className="text-red-500">{meta.error}</div> : <></>}
            {(customErrorLabel?.text && meta.touched && meta.error) || (customErrorLabel?.text && alwaysShowErrorLabel) ? (
              <div className="flex w-full justify-between">
                <div className="text-red-500">{meta.error}</div>
                <button type="button" onClick={() => customErrorLabel?.action()}>
                  {customErrorLabel?.text}
                </button>
              </div>
            ) : <></>}
          </div>
        );
      }}
    </Field>
  );
};
