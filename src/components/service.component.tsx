import { useState } from 'react';
import { DButton } from './dbButton.component';
import { EditStack } from './edit-modal.component';
import { Popconfirm, message } from 'antd';
import { useRouter } from 'next/router';

interface PackageProps {
  id: number;
  name: string;
  category: string;
  description: string;
  unitCost: string;
  stack: any;
  setStack: (data: any) => void;
  onDelete: (id: number) => void;
}

export const Service: React.FC<PackageProps> = ({
  id = 1,
  name = '',
  category = '',
  description = '',
  unitCost = 12,
  stack,
  setStack,
  onDelete,
}) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const [editedData, setEditedData] = useState({
    id,
    name,
    category,
    description,
    unitCost,
  });

  const router = useRouter();

  const openEditModal = () => {
    setIsModalOpen(true);
  };

  const closeEditModal = () => {
    setIsModalOpen(false);
  };

  const saveChanges = (updatedData: any) => {
    console.log(updatedData?.id);
    // const oldItems = stack?.stackItems?.filter((item: any) => item?.id !== editedData?.id);
    // const items = [...oldItems, { ...updatedData, unitCost: Number(updatedData?.unitCost), quantity: Number(updatedData?.quantity) }];
    // // console.log(oldItems); // const newItems = {...oldItems, updatedData}
    // setStack({
    //   ...stack,
    //   stackItems: items.sort((a, b) => {
    //     return a.id - b.id;
    //   }),
    // });
    // setEditedData(updatedData);
    // closeEditModal();
  };

  const handleDelete = () => {
    message.success('Deleted successfully');
    onDelete(id);
  };

  return (
    <tbody>
      <tr className="text-[14px] font-[500]">
        {/* <th scope="row" className="px-6 py-4 whitespace-nowrap">
          {id + 1}
        </th> */}
        <td className="px-6 py-4">{editedData.name}</td>
        <td className="px-6 py-4">{editedData.category}</td>
        <td className="px-6 py-4">{editedData.description}</td>
        <td className="px-6 py-4">${editedData.unitCost}</td>
        <td className="flex px-2 py-4">
          <DButton type="button" onClick={() => { router.push('/services') }}>
            <img className="w-4 h-4" src="/icon/pencil.svg" alt="pencil" />
          </DButton>
          <Popconfirm title="Are you sure you want to delete this row?" onConfirm={handleDelete} okText="Yes" cancelText="No">
            <DButton type="button">
              <img className="w-4 h-4" src="/icon/bin.svg" alt="bin" />
            </DButton>
          </Popconfirm>
        </td>
      </tr>

      {/* Edit Modal */}
      {isModalOpen && <EditStack onClose={closeEditModal} onSave={saveChanges} initialData={editedData} setStack={setStack} />}
    </tbody>
  );
};
