import { Dot } from "./dot.component";

export const PayChartDetail = ({
  company = "Demo Company",
  color = "#F4BE37",
}) => {
  return (
    <div className="flex flex-col gap-[2px]">
      <div className="flex gap-[16px] items-center">
        <Dot color={color} />
        <p className="text-gray-400 text-[12px]">{company}</p>
      </div>
    </div>
  );
};
