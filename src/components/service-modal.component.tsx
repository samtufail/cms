import { useState } from 'react';
import { DButton } from './dbButton.component';
import { CustomSelect } from '.';
import { Select } from 'antd';
import { Form, Formik } from 'formik';

const { Option } = Select;

export const ServiceModal = ({ onClose, stack, setStack, uniqueItems }: any) => {
  const [serviceData, setServiceData] = useState({
    name: '',
    category: '',
    description: '',
    unitCost: '',
  });

  const handleSave = () => {
    // Validation
    if (!serviceData.name || !serviceData.category || !serviceData.description || !serviceData.unitCost) {
      console.log('empty');
      return;
    }
    //New one
    const newService = {
      id: stack.stackItems?.length || 0,
      ...serviceData,
      unitCost: parseFloat(serviceData.unitCost),
    };
    //update
    const initialStackItems = stack.stackItems || [];
    const updatedStack = {
      ...stack,
      stackItems: [...initialStackItems, newService].sort((a: any, b: any) => a.id - b.id),
    };

    setStack(updatedStack);
    onClose();
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>, field: string) => {
    const value = e.target.value;
    setServiceData((prevData: any) => ({
      ...prevData,
      [field]: value,
    }));
  };


  const handlePreChange = (e: string, field: string) => {
    setServiceData((prevData: any) => ({
      ...prevData,
      [field]: e,
    }));
  };

  return (
    <Formik initialValues={{}} onSubmit={(values) => { }}>
      {({ values, setFieldValue }) => (
        <Form>
          <div className="fixed inset-0 flex items-center justify-center z-50">
            <div className="fixed inset-0 bg-black opacity-50"></div>
            <div className="z-10 bg-[#fff] w-[430px] rounded-lg py-[15px] px-[12px]">
              <h2 className="text-2xl font-semibold mb-4 mt-[12px]">New Service</h2>
              <div className="flex flex-col gap-[6px]">
                <CustomSelect
                  onChange={(data: any) => {
                    if (data) {
                      const item = uniqueItems[data - 1];
                      // Set Data in Input Fields
                      handlePreChange(item?.name, 'name');
                      handlePreChange(item?.category, 'category');
                      handlePreChange(item?.description, 'description');
                      handlePreChange(item?.unitCost, 'unitCost');
                    }
                  }}
                  name="stack"
                  type="stack"
                  label="Select Service"
                  placeholder="Select From Pre-made Services"
                >
                  {uniqueItems?.map((item: any, index: number) => (
                    <Option key={index + 1} value={index + 1}>
                      {item?.name}
                    </Option>
                  ))}
                </CustomSelect>
              </div>
              <div className="flex flex-col gap-[6px] mt-2">
                <label className='text-[14px]'>Service Name</label>
                <input
                  type="text"
                  title="name"
                  className="border border-gray-300 p-2 rounded-md w-full"
                  value={serviceData.name}
                  onChange={(e) => handleChange(e, 'name')}
                />
              </div>
              <div className="flex flex-col gap-[6px] mt-2">
                <label className='text-[14px]'>Category</label>
                <input
                  type="text"
                  title="category"
                  className="border border-gray-300 p-2 rounded-md w-full"
                  value={serviceData.category}
                  onChange={(e) => handleChange(e, 'category')}
                />
              </div>
              <div className="flex flex-col gap-[6px] mt-2">
                <label className='text-[14px]'>Description</label>
                <input
                  type="text"
                  title="description"
                  className="border border-gray-300 p-2 rounded-md w-full"
                  value={serviceData.description}
                  onChange={(e) => handleChange(e, 'description')}
                />
              </div>
              <div className="flex flex-col gap-[6px] mt-2">
                <label className='text-[14px]'>Unit Cost</label>
                <input
                  type="number"
                  title="unitcost"
                  className="border border-gray-300 p-2 rounded-md w-full"
                  value={serviceData.unitCost}
                  onChange={(e) => handleChange(e, 'unitCost')}
                />
              </div>
              <div className="mt-4 flex justify-end gap-[4px]">
                <DButton onClick={onClose} className="bg-[#909090] text-[#fff] px-[16px] rounded-lg">
                  Cancel
                </DButton>
                <DButton onClick={handleSave} className="bg-[#4F46E5] text-[#fff] w-[120px] px-[12px] rounded-lg">
                  Save Changes
                </DButton>
              </div>
            </div>
          </div>
        </Form>)}
    </Formik>
  );
};
