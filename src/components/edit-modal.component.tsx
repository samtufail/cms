import { useState } from 'react';
import { DButton } from './dbButton.component';


export const EditStack = ({ onClose, onSave, initialData }: any) => {
  const [editedData, setEditedData] = useState(initialData);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>, field: string) => {
    setEditedData((prevData: any) => ({
      ...prevData,
      [field]: event.target.value,
    }));
  };

  const saveChanges = () => {
    onSave(editedData);
  };

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50">
      <div className="fixed inset-0 bg-black opacity-50"></div>
      <div className="z-10 bg-[#fff] w-[430px] rounded-lg py-[15px] px-[12px]">
        <h2 className="text-2xl font-semibold mb-4 mt-[12px]">Edit Stack Name</h2>
        <div className="flex flex-col gap-[6px]">
          <label>Service Name</label>
          <input
            type="text"
            title="Service Name"
            className="border border-gray-300 p-2 rounded-md w-full"
            value={editedData.name}
            onChange={(e) => handleInputChange(e, 'name')}
          />
        </div>
        <div className="flex flex-col gap-[6px] mt-2">
          <label>Category</label>
          <input
            type="text"
            title="Category"
            className="border border-gray-300 p-2 rounded-md w-full"
            value={editedData.category}
            onChange={(e) => handleInputChange(e, 'category')}
          />
        </div>
        <div className="flex flex-col gap-[6px] mt-2">
          <label>Product Description</label>
          <input
            type="text"
            title="Product Description"
            className="border border-gray-300 p-2 rounded-md w-full"
            value={editedData.description}
            onChange={(e) => handleInputChange(e, 'description')}
          />
        </div>
        <div className="flex flex-col gap-[6px] mt-2">
          <label>Unit Cost</label>
          <input
            type="number"
            title="Unit Cost"
            className="border border-gray-300 p-2 rounded-md w-full"
            value={editedData.unitCost}
            onChange={(e) => handleInputChange(e, 'unitCost')}
          />
        </div>
        <div className="mt-4 flex justify-end gap-[4px]">
          <DButton onClick={onClose} className="bg-[#909090] text-[#fff] px-[16px] rounded-lg">
            Cancel
          </DButton>
          <DButton onClick={saveChanges} className="bg-[#4F46E5] text-[#fff] w-[120px] px-[12px] rounded-lg">
            Edit
          </DButton>
        </div>
      </div>
    </div>
  );
};
