import { MouseEventHandler, ReactNode } from 'react';

export const DButton = ({
  children,
  type = 'button',
  onClick,
  className = '',
  loading = false,
}: {
  children: ReactNode;
  type?: 'submit' | 'button';
  onClick?: MouseEventHandler<HTMLButtonElement> | undefined;
  className?: string;
  loading?: boolean;
}) => {
  return (
    <button type={type} className={`flex justify-center items-center p-[10px] gap-[2px]  ${className}`} onClick={onClick}>
      {loading ? children : children}
    </button>
  );
};
