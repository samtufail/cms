import { Dot } from "./dot.component";

export const ChartDetails = ({
  name = "Demo Company",
  value = 1234.14,
  color = "#F4BE37",
}) => {

  return (
    <div className="flex flex-col gap-[3px]">
      <div className="flex gap-[16px] items-center">
        <Dot color={color} />
        <p className="text-gray-400 text-[12px]">{name}</p>
      </div>
      <p className="text-[#000] text-[16px] font-[600] ml-[24px]">${value}</p>
    </div>
  );
};
