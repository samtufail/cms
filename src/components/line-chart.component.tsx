import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

const data = [
  {
    name: "Jan",
    mr: 3000,
    time: 2200,
    amt: 5400,
  },
  {
    name: "Feb",
    mr: 3000,
    time: 2500,
    amt: 2210,
  },
  {
    name: "Mar",
    mr: 2500,
    time: 2100,
    amt: 2290,
  },
  {
    name: "Apr",
    mr: 5700,
    time: 2500,
    amt: 2000,
  },
  {
    name: "May",
    mr: 5700,
    time: 6200,
    amt: 2181,
  },
  {
    name: "Jun",
    mr: 4500,
    time: 2300,
    amt: 2500,
  },
  {
    name: "July",
    mr: 4800,
    time: 1400,
    amt: 2100,
  },
  {
    name: "Aug",
    mr: 4800,
    time: 1600,
    amt: 2100,
  },
  {
    name: "Sep",
    mr: 4800,
    time: 1400,
    amt: 2100,
  },
  {
    name: "Oct",
    mr: 2800,
    time: 1200,
    amt: 2100,
  },
  {
    name: "Nov",
    mr: 5800,
    time: 1100,
    amt: 1100,
  },
  {
    name: "Dec",
    mr: 1800,
    time: 1130,
    amt: 1100,
  },
  {
    name: "Jan",
    mr: 3000,
    time: 2200,
    amt: 5400,
  },
  {
    name: "Feb",
    mr: 3000,
    time: 2500,
    amt: 2210,
  },
  {
    name: "Mar",
    mr: 2500,
    time: 2100,
    amt: 2290,
  },
  {
    name: "Apr",
    mr: 5700,
    time: 2500,
    amt: 2000,
  },
  {
    name: "May",
    mr: 5700,
    time: 6200,
    amt: 2181,
  },
  {
    name: "Jun",
    mr: 4500,
    time: 2300,
    amt: 2500,
  },
  {
    name: "July",
    mr: 4800,
    time: 1400,
    amt: 2100,
  },
  {
    name: "Aug",
    mr: 4800,
    time: 1600,
    amt: 2100,
  },
  {
    name: "Sep",
    mr: 4800,
    time: 1400,
    amt: 2100,
  },
  {
    name: "Oct",
    mr: 2800,
    time: 1200,
    amt: 2100,
  },
  {
    name: "Nov",
    mr: 5800,
    time: 1100,
    amt: 1100,
  },
  {
    name: "Dec",
    mr: 1800,
    time: 1130,
    amt: 1100,
  },
];

export const LinesChart = () => {
  return (
    <div className="grid md:grid-col-4 grid-cols-1 py-12">
      <h2 className="text-[20px] font-[700] ml-12">Total MRR</h2>
      <div className="overflow-hidden">
        <LineChart
          width={1000}
          height={300}
          data={data}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Line
            type="monotone"
            dataKey="mr"
            stroke="#8884d8"
            activeDot={{ r: 1 }}
          />
          <Line type="monotone" dataKey="time" stroke="#82ca9d" />
        </LineChart>
      </div>
    </div>
  );
};
