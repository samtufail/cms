export const Dot = ({ color = '#F4BE37' }) => {
  return (
    <div className="flex">
      <div className="w-2 h-2 rounded-full" style={{ background: color }}></div>
    </div>
  );
};
