import { ArrowUpIcon } from "@heroicons/react/24/outline";

export const MatrixCard = ({
  title = "Total Monthly Recurring Revenue",
  price = "$32,897",
  increase = "$2000.00",
  icon = "/icon/dollar.svg",
}) => {
  return (
    <div className="py-8">
      <div className="flex flex-row gap-[12px] items-center mt-4">
        <img
          className="w-[50px] h-[50px] p-4 bg-[#6366F1] text-[#fff] rounded"
          src={icon}
          alt="icon"
        />
        <div className="flex flex-col text-[#111827]">
          <p className="text-[#6B7280] font-[500] text-[14px]">{title}</p>
          <h5 className="text-[24px] font-[600]">
            {price}
            <ArrowUpIcon className="w-5 h-5 text-[#10B981] inline ml-3" />
            <span className="text-[#10B981] font-[500] text-[14px]">
              {increase}
            </span>
          </h5>
        </div>
      </div>
      <button className="mt-[23px] text-indigo-600">View all</button>
    </div>
  );
};
