import { PieChart, Pie, Cell } from 'recharts';

const data = [
  { name: 'Demo Company 1', value: 90, color: '#5388D8' },
  { name: 'Demo Company 1', value: 140, color: '#0D2535' },
  { name: 'Demo Company 1', value: 30, color: '#FF9F40' },
  { name: 'Demo Company 1', value: 120, color: '#F4BE37' },
];

export const Chart = ({
  chartData = [
    { name: 'Demo Company 1', value: 90, color: '#5388D8' },
    { name: 'Demo Company 1', value: 140, color: '#0D2535' },
    { name: 'Demo Company 1', value: 30, color: '#FF9F40' },
    { name: 'Demo Company 1', value: 120, color: '#F4BE37' },
  ],
}: any) => {
  const COLORS = chartData?.map((entry: any) => entry?.color);
  return (
    <div className="flex items-center justify-center">
      <PieChart width={200} height={200}>
        <Pie
          data={chartData}
          dataKey="value"
          nameKey="name"
          cx="50%"
          cy="50%"
          outerRadius={100}
          fill="#8884d8"
          stroke="none"
          strokeWidth={0}
        >
          {data.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={COLORS[index]} />
          ))}
        </Pie>
      </PieChart>
    </div>
  );
};
