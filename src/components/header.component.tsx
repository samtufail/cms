import { timeOfDay } from '@/utils';

export const Header = () => {
  return (
    <header className="py-10">
      <div className="mx-auto max-w-[98rem] px-4 sm:px-6">
        <h1 className="text-3xl font-bold tracking-tight text-white ml-[24px]">Good {timeOfDay()}, Mitch.</h1>
      </div>
    </header>
  );
};
