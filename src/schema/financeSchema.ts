import * as yup from "yup";

export const finance = {
  schema: yup.object().shape({
    startDate: yup.string().required("Date is required!"),
    endDate: yup.string().required("Date is required!"),
    startTime: yup.string().required("required!"),
    endTime: yup.string().required("required!"),
  }),
  initialValues: {
    startDate: "",
    endDate: "",
    startTime: "",
    endTime: "",
  },
};
