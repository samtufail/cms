import * as yup from 'yup';

export const contract = {
  schema: yup.object().shape({
    companyName: yup.string().required('Company Name is required!'),
    // stack: yup.string().required('Stack is Required!'),
    term: yup.string().required('Term is Required!'),
    startDate: yup.string().required('Date is required!'),
    locations: yup.string().required('Location is required!'),
    computerUsers: yup.string().required('Computer User is required!'),
    workStations: yup.string().required('Computer User is required!'),
    emailOnlyUsers: yup.string().required('Email only user is required!'),
    servers: yup.string().required('Servers is required!'),
  }),
  initialValues: {
    companyName: '',
    stack: '',
    mrr: '',
    startDate: '',
    term: 0,
    endDate: '',
    locations: '',
    computerUsers: '',
    workStations: '',
    totalContractValue: '',
    emailOnlyUsers: '',
    servers: '',
    margin: 67,
  },
};
