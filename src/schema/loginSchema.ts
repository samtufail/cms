import * as yup from "yup";

export const login = {
  schema: yup.object().shape({
    email: yup.string().email().required("Email is required!"),
    password: yup.string().required("Password is required!"),
  }),
  initialValues: {
    email: "",
    password: "",
  },
};

export const reset = {
  schema: yup.object().shape({
  email: yup.string().email().required("Email is required!"),
  }),
  initialValues: {
    email: "",
  },
};

export const resetPass = {
  schema: yup.object().shape({
    password: yup.string().required("Password is required!"),
    confirmPass: yup
      .string()
      .label("Confirm Password")
      .required()
      .oneOf([yup.ref("password")], "Password must match"),
  }),
  initialValues: {
    password: "",
    confirmPass: "",
  },
};
