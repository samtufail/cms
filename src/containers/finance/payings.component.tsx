import { Fragment } from "react";
import { Paying } from "@/components";
import payData from "@/data/pay.json";

export const Payings = () => {
  return (
    <>
      {payData.map((pay, index) => (
        <Fragment key={index}>
          <Paying {...pay} />
        </Fragment>
      ))}
    </>
  );
};
