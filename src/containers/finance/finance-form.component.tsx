import { DButton, DatePicker, TimePicker } from "@/components";
import { Formik, Form } from "formik";
import { finance } from "@/schema";
import { ArrowDownCircleIcon } from "@heroicons/react/24/outline";

export function FinanceForm() {
  return (
    <div className="mt-2">
      <Formik
        initialValues={finance.initialValues}
        validationSchema={finance.schema}
        onSubmit={(values) => {
          // Same shape as initial values
          console.log(values);
        }}
      >
        {({ errors, touched }) => (
          <Form>
            <div className="grid md:grid-cols-4 gap-[12px] items-center">
              <div className="flex items-end gap-[4px]">
                <DatePicker
                  name="startDate"
                  type="date"
                  label="From Date"
                  placeholder="MM-DD-YYYY"
                  className="border p-2 rounded-md"
                  style={{ height: "20px", width: "420px" }}
                />
                <TimePicker
                  name="startTime"
                  type="time"
                  label="From Time"
                  placeholder="Set Start Timer"
                  className="border p-2 rounded-md"
                />
              </div>
              <div className="flex items-end gap-[4px]">
                <DatePicker
                  name="endDate"
                  type="date"
                  label="To Date"
                  placeholder="MM-DD-YYYY"
                  className="border p-2 rounded-md"
                  style={{ height: "20px", width: "420px" }}
                />
                <TimePicker
                  name="endTime"
                  type="time"
                  label="To Time"
                  placeholder="Set End Timer"
                  className="border p-2 rounded-md"
                />
              </div>
              {/* Button */}
              <DButton
                className="bg-indigo-600 text-white h-9 w-[220px] px-3 rounded-lg"
                type="submit"
              >
                <ArrowDownCircleIcon className="inline w-5 h-5" />
                Export Financial Report
              </DButton>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
}
