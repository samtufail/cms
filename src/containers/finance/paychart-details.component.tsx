import { Fragment } from "react";
import { PayChartDetail } from "@/components";
import chartData from "@/data/paychart.json";

export const PayChartDetails = () => {
  return (
    <section className="flex flex-col justify-center">
      {chartData.map((pay, index) => (
        <Fragment key={index}>
          <PayChartDetail {...pay} />
        </Fragment>
      ))}
    </section>
  );
};
