import { Fragment } from "react";
import { MatrixCard } from "@/components";
import matrixData from "@/data/matrix.json";

export const MatrixCards = () => {
  return (
    <div className="flex md:flex-row flex-col md:gap-[8rem] px-[22px]">
      {matrixData.map((matrix, index) => (
        <Fragment key={index}>
          <MatrixCard {...matrix} />
        </Fragment>
      ))}
    </div>
  );
};
