import { LinesChart, Chart } from "@/components";
import { Matrics } from "./matrics.component";
import { FinanceForm } from "./finance-form.component";
import { Payings } from "./payings.component";
import { PayChartDetails } from "./paychart-details.component";

export const Finance = () => {
  return (
    <div className="-mt-2 border-[#CECECE]">
      <div className="mx-auto max-w-[98rem] px-4 pb-6 sm:px-6">
        <div className="rounded-lg bg-white text-[#071638] px-5 py-6 shadow sm:px-6">
          <div className="flex flex-col w-full">
            {/* header */}
            <div className="flex md:flex-row flex-col gap-[12px]">
              <h2 className="text-[24] font-[500]">Finances</h2>
            </div>
            {/* Form */}
            <FinanceForm />
            {/* MRR */}
            <div className="flex flex-wrap gap-[24px]">
              <LinesChart />
              <Chart />
              <PayChartDetails />
            </div>
            {/* Matrix  */}
            <Matrics />
            {/* Paying Contracts Table */}
            <div className="flex md:flex-row flex-col gap-[12px]">
              <h2 className="text-[24px] font-[500]">
                Highest-Paying Contracts
              </h2>
            </div>
            <div className="relative overflow-x-auto shadow-md sm:rounded-lg py-[17px]">
              <table className="w-full text-left">
                <thead className="text-[12px] text-[#6B7280] uppercase ">
                  <tr>
                    <th scope="col" className="px-6 py-3">
                      SR #
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Comopany Name
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Monthly Recurring Revenue
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Contract Value
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Status
                    </th>
                  </tr>
                </thead>
                <Payings />
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
