import { Fragment } from 'react';
import { StackCard } from '@/components';

export const StackCards = ({ stacks, contracts }: { stacks: any; contracts: any }) => {
  return (
    <section className="grid md:grid-cols-4 grid-cols-1 w-full">
      {stacks.map((stack: any, index: number) => (
        <Fragment key={index}>
          <StackCard {...stack} contracts={contracts} />
        </Fragment>
      ))}
    </section>
  );
};
