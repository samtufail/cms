import { useEffect, useState } from 'react';
import { DButton, CustomMultiSelect } from '@/components';
import { formatCurrency } from '@/utils';
import { Select, message } from 'antd';
import { Form, Formik } from 'formik';

const { Option } = Select;

export const AddItem = ({ onClose, stack, setStack, allItems }: any) => {
  const [selected, setSelected] = useState<string[]>(['']);
  const [items, setItems] = useState([]);
  const handleSave = () => {
    // Validation
    if (!selected) {
      message.error('Please select a service!');
      return;
    }
    console.log(selected);
    const selectedItems = allItems?.filter((item: any) => {
      if (selected?.length) {
        return selected.includes(item?.id);
      }
    });
    console.log(selectedItems);
    const prevItems = stack?.stackItems ? stack?.stackItems : [];
    const newStack = {
      ...stack, stackItems: [...prevItems, ...selectedItems].sort(function (a: any, b: any) {
        if (a?.categoryName < b?.categoryName) { return -1; }
        if (a?.categoryName > b?.categoryName) { return 1; }
        return 0;
      })
    }
    setStack(newStack);
    onClose();
  };

  useEffect(() => {
    const filteredItems = allItems?.filter((item: any) => {
      if (stack?.stackItems?.length) {
        const finalItem = stack?.stackItems?.find((sItem: any) => sItem?.id === item?.id);
        if (finalItem?.id) {
          return false
        } else {
          return true
        }
      } else {
        return true
      }
    });
    filteredItems?.sort(function (a: any, b: any) {
      if (a?.categoryName < b?.categoryName) return -1;
      if (a?.categoryName > b?.categoryName) return 1;
      if (a?.categoryName === b?.categoryName) {
        if (a?.name < b?.name) return -1
        if (b?.name > a?.name) return 1
      }
      return 0;
    });
    setItems(filteredItems);
  }, [allItems, stack?.stackItems])

  return (
    <Formik initialValues={{}} onSubmit={(values) => { }}>
      {({ values, setFieldValue }) => (
        <Form>
          <div className="fixed inset-0 flex items-center justify-center z-50">
            <div className="fixed inset-0 bg-black opacity-50"></div>
            <div className="z-10 bg-[#fff] w-[550px] rounded-lg py-[15px] px-[12px]">
              <h2 className="text-2xl font-semibold mb-4 mt-[12px]">New Service</h2>
              <div className="flex flex-col gap-[6px]">
                <CustomMultiSelect
                  onChange={(data: any) => {
                    setSelected(data);
                  }}
                  name="stack"
                  type="stack"
                  label="Select Service"
                  placeholder="Select From Pre-made Services"
                >
                  {items?.map((item: any, index: number) => (
                    <Option key={item?.id} value={item?.id}>
                      {item?.name} | {formatCurrency(item?.unitCost)} | {item?.categoryName}
                    </Option>
                  ))}
                </CustomMultiSelect>
              </div>
              <div className="mt-4 flex justify-end gap-[4px]">
                <DButton onClick={onClose} className="bg-[#909090] text-[#fff] px-[16px] rounded-lg">
                  Cancel
                </DButton>
                <DButton onClick={handleSave} className="bg-[#4F46E5] text-[#fff] w-[120px] px-[12px] rounded-lg">
                  Add Item(s)
                </DButton>
              </div>
            </div>
          </div>
        </Form>)}
    </Formik>
  );
};
