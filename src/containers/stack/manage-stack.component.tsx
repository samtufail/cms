import { useEffect, useState } from "react";
import { PencilSquareIcon, PlusCircleIcon } from "@heroicons/react/24/outline";
import { Services } from "./services.component";
import { StackModal, DButton } from "@/components";
import { useRouter } from "next/router";
import { getById } from "@/api/stack";
import { formatCurrency } from "@/utils";
import { getAll as getAllItems, update as updateItem } from "@/api/stack-item";
import { getAll as getAllCat } from "@/api/category";
import { update } from "@/api/stack";
import { AddItem } from "./add-item.component";
import { EditItem } from "./edit-item.component";
import { Popconfirm } from "antd";
import { serverTimestamp } from "firebase/firestore";

export const ManageStack = () => {
  const [addModal, setAddModal] = useState(false);
  const [edit, setEdit] = useState(false);
  const [isStackOpen, setIsStackOpen] = useState(false);
  const [stack, setStack] = useState<any>({});
  const [allItems, setAllItems] = useState<any>([]);
  const [current, setCurrent] = useState<any>({});
  const [categories, setCategories] = useState<any>([]);

  const openStackModal = () => {
    setIsStackOpen(true);
  };

  const router = useRouter();

  useEffect(() => {
    (async () => {
      const initialStack = await getById(router?.query?.id as string);
      const stackItems = await getAllItems();
      const categories = await getAllCat();
      setCategories(categories);
      if (stackItems?.length) {
        const itemsWithCategory = stackItems?.map((item: any) => {
          return {
            ...item,
            categoryName: categories?.filter(
              (category: any) => category?.id === item?.category,
            )[0]?.name,
          };
        });
        setAllItems(itemsWithCategory);
        const stacksStackItems = initialStack?.stackItems
          ?.map((item: any) => {
            const sItem = itemsWithCategory?.filter(
              (sItem: any) => sItem?.id === item,
            )[0];
            return sItem;
          })
          .sort(function (a: any, b: any) {
            if (a?.categoryName < b?.categoryName) {
              return -1;
            }
            if (a?.categoryName > b?.categoryName) {
              return 1;
            }
            return 0;
          });
        const finalItems = stacksStackItems?.filter((item: any) => item?.id ? true : false);
        const stack = { ...initialStack, stackItems: finalItems };
        setStack(stack);
      }
    })();
  }, [router]);

  const handleSave = async () => {
    const finalStack = {
      ...stack,
      stackItems: stack?.stackItems?.map((item: any) => item?.id),
    };
    await update(stack.id, finalStack);
    router.reload();
  };

  // Edit Service
  const editService = async ({ id, values }: any) => {
    await updateItem(id, {
      ...values,
      description: values?.description || "",
      updatedAt: serverTimestamp(),
    });
    const items = await getAllItems();
    if (items?.length) {
      router.reload();
    }
  };

  // Delete Service
  const handleDelete = (id: any) => {
    // Find and remove the package with the specified 'sr' from the data
    const newItems = stack?.stackItems.filter((data: any) => data.id !== id);
    // Update the state with the modified data
    setStack({ ...stack, stackItems: newItems });
  };

  return (
    <div className="-mt-2 border-[#CECECE]">
      <div className="mx-auto max-w-[98rem] px-4 pb-6 sm:px-6">
        <div className="rounded-lg bg-white px-5 py-6 text-[#071638] shadow sm:px-6">
          <div className="flex w-full flex-col">
            {/* Header */}
            <div className="flex flex-col justify-between gap-[12px] md:flex-row">
              <div className="flex flex-col items-center justify-center gap-[16px] text-center md:flex-row ">
                <h2 className="text-[24px] font-[700] md:text-[30px]">
                  {stack?.name}
                </h2>
                <DButton
                  type="button"
                  className="border-grey-200 flex items-center justify-center border text-[14px] font-[500] shadow-md"
                  onClick={openStackModal}
                >
                  <PencilSquareIcon
                    className="block h-4 w-4"
                    aria-hidden="true"
                  />
                  <div>Edit Template Name</div>
                </DButton>
              </div>
              <DButton
                type="button"
                onClick={() => setAddModal(true)}
                className="flex items-center justify-center shadow-md"
              >
                <PlusCircleIcon className="block h-6 w-6" aria-hidden="true" />
                <div>Add New Service</div>
              </DButton>
            </div>
            {addModal && (
              <AddItem
                stack={stack}
                onClose={() => setAddModal(false)}
                setStack={setStack}
                allItems={allItems}
              />
            )}
            {isStackOpen && (
              <StackModal
                stack={stack}
                cancelButton={() => setIsStackOpen(false)}
                isAdd={false}
              />
            )}
            {edit && current?.name && (
              <EditItem
                editService={editService}
                setModal={setEdit}
                stackItem={current}
                setCurrent={setCurrent}
                categories={categories}
              />
            )}
            {/* Table  */}
            <div className="relative overflow-x-auto pt-[22px] shadow-md sm:rounded-lg">
              <table className="w-full text-left ">
                <thead className="text-[12px] uppercase text-[#6B7280] ">
                  <tr>
                    <th scope="col" className="px-6 py-3">
                      ID
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Name
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Description
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Category
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Unit Cost
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {stack?.stackItems?.map((item: any) => {
                    if (item?.id) {
                      return (
                        <tr className="text-[14px] font-[500]" key={item?.id}>
                          <td className="px-6 py-4">{item?.id}</td>
                          <td className="px-6 py-4">{item?.name}</td>
                          <td className="px-6 py-4">
                            {item?.description ? item?.description : "-"}
                          </td>
                          <td className="px-6 py-4">{item?.categoryName}</td>
                          <td className="px-6 py-4">
                            {formatCurrency(item?.unitCost)}
                          </td>
                          <td className="flex px-2 py-4">
                            <DButton
                              type="button"
                              onClick={() => {
                                setCurrent(item);
                                setEdit(true);
                              }}
                            >
                              <img
                                className="h-4 w-4"
                                src="/icon/pencil.svg"
                                alt="pencil"
                              />
                            </DButton>
                            <Popconfirm
                              title="Are you sure you want to delete this row?"
                              onConfirm={() => handleDelete(item?.id)}
                              okText="Yes"
                              cancelText="No"
                            >
                              <DButton type="button">
                                <img
                                  className="h-4 w-4"
                                  src="/icon/bin.svg"
                                  alt="bin"
                                />
                              </DButton>
                            </Popconfirm>
                          </td>
                        </tr>
                      );
                    }
                  })}
                </tbody>
              </table>
            </div>
            <div className="mt-8 flex justify-end gap-[12px]">
              <DButton
                type="button"
                className="w-[110px] bg-[#909090] text-[#fff] shadow-md"
                onClick={() => router.push("/stack")}
              >
                <div>Cancel</div>
              </DButton>
              <DButton
                type="button"
                className="w-[140px]  bg-[#4F46E5] text-[#fff] shadow-md"
                onClick={handleSave}
              >
                <div>Save</div>
              </DButton>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
