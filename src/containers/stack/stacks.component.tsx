import { PlusCircleIcon } from '@heroicons/react/24/outline';
import { DButton } from '@/components';
import { StackCards } from './stack-cards.component';
import { useState } from 'react';
import { StackModal } from '@/components';

export const Stacks = ({ stacks, contracts }: { stacks: any; contracts: any }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const openStackModal = () => {
    setIsModalOpen(true);
  };

  return (
    <div className="-mt-2 border-[#CECECE]">
      <div className="mx-auto max-w-[98rem] px-4 pb-6 sm:px-6">
        <div className="rounded-lg bg-white text-[#071638] px-5 py-6 shadow sm:px-6">
          <div className="flex flex-col w-full">
            {/* Header */}
            <div className="flex md:flex-row flex-col gap-[12px] justify-between">
              <div className="flex md:flex-row flex-col text-center items-center justify-center gap-[6px]">
                <h2 className="md:text-[30px] text-[25px] font-[700]">Stack Templates</h2>
                {/* <span className="md:text-[18px] text-[14px] font-[700]">- Explore and manage your service packages.</span> */}
              </div>
              <DButton type="button" onClick={openStackModal} className="flex items-center justify-center shadow-md">
                <PlusCircleIcon className="block h-6 w-6" aria-hidden="true" />
                <div>Add New Stack</div>
              </DButton>
            </div>
            {/* Add Modal */}
            {isModalOpen && <StackModal stack={{ name: '' }} isAdd cancelButton={() => setIsModalOpen(false)} />}
            {/* Stack Cards */}
            <div className="flex md:flex-row flex-col gap-[12px] w-full items-center">
              <StackCards stacks={stacks} contracts={contracts} />
            </div>
            <div className="text-[12px] mt-[20px]">
              (*) Stack templates that are being used in contracts cannot be deleted, please change stack inside contracts to enable
              deletion of a specific Stack template. Thanks!
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
