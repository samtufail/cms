import { Service } from '@/components';

export const Services = ({ stack, setStack }: { stack: any; setStack: any }) => {
  const handleDelete = (id: any) => {
    // Find and remove the package with the specified 'sr' from the data
    const newItems = stack?.stackItems.filter((data: any) => data.id !== id);
    // Update the state with the modified data
    setStack({ ...stack, stackItems: newItems });
  };


  return (
    <>
      {stack?.stackItems?.map((service: any, index: number) => (
        <Service
          key={service?.name}
          onDelete={handleDelete}
          id={service?.id}
          name={service?.name}
          category={service?.categoryName}
          description={service?.description || "-"}
          unitCost={service?.unitCost}
          setStack={setStack}
          stack={stack}
        />
      ))}
    </>
  );
};
