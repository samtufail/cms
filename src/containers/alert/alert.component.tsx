import { AlertDetails } from "./alert-details.component";

export const Alert = () => {
  return (
    <div className="-mt-2 border-[#CECECE]">
      <div className="mx-auto max-w-[98rem] px-4 pb-6 sm:px-6">
        <div className="rounded-lg bg-white text-[#000000] px-5 py-6 shadow sm:px-6">
          {/* Header */}
          <div className="flex gap-[12px] justify-between">
            <h3 className="text-[24px] font-[600]">Critical Alerts</h3>
          </div>
          <AlertDetails />
        </div>
      </div>
    </div>
  );
};
