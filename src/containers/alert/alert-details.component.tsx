import { Fragment } from "react";
import { AlertDetail } from "@/components";
import alertData from "@/data/alert.json";

export const AlertDetails = () => {
  return (
    <section className="flex flex-col gap-[8px] w-full flex-wrap pt-[36px] text-[16px] font-[500]">
      {alertData.map((alert, index) => (
        <Fragment key={index}>
          <AlertDetail {...alert} />
        </Fragment>
      ))}
    </section>
  );
};
