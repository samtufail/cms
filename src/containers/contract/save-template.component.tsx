import { update } from '@/api/contract';
import { add } from '@/api/stack';
import { DButton, Input } from '@/components';
import { Select } from 'antd';
import { Form, Formik } from 'formik';
import { useRouter } from 'next/router';
import { useState } from 'react';

const { Option } = Select;

export const SaveTemplate = ({ onClose, items, id }: any) => {
  const [name, setName] = useState(false);
  const router = useRouter();
  const handleSave = async () => {
    const finalItems = items?.length ? items?.map((item: any) => {
      return item?.id
    }) : []
    const finalObject = {
      stackItems: finalItems,
      name: name ? name : '',
    }
    const stack = await add(finalObject);
    await update(id, {stack, additionalItems: [], deletedItems: []});
    router.reload();
  }
  return (
    <Formik initialValues={{}} onSubmit={(values) => { }}>
      {({ values, setFieldValue }) => (
        <Form>
          <div className="fixed inset-0 flex items-center justify-center z-50">
            <div className="fixed inset-0 bg-black opacity-50"></div>
            <div className="z-10 bg-[#fff] w-[550px] rounded-lg py-[15px] px-[12px]">
              <h2 className="text-2xl font-semibold mb-4 mt-[12px]">Save Template</h2>
              <div className="flex flex-col gap-[6px]">
                <Input name="templateName" placeholder="Enter Template Name" customOnChange={(text: any) => {
                  setName(text);
                }} />
              </div>
              <div className="mt-4 flex justify-end gap-[4px]">
                <DButton onClick={onClose} className="bg-[#909090] text-[#fff] px-[16px] rounded-lg">
                  Cancel
                </DButton>
                <DButton onClick={handleSave} className="bg-[#4F46E5] text-[#fff] w-[150px] px-[12px] rounded-lg">
                  Save Template
                </DButton>
              </div>
            </div>
          </div>
        </Form>)}
    </Formik>
  );
};
