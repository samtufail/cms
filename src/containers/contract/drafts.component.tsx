import { ArrowLeftIcon, PlusCircleIcon } from '@heroicons/react/24/outline';
import { DButton } from '@/components';
import { useRouter } from 'next/router';
import { DraftContract } from '../dashboard';

export const Drafts = ({ drafts }: any) => {
  const router = useRouter();

  return (
    <div className="-mt-2 border-[#CECECE]">
      <div className="mx-auto max-w-[98rem] px-4 pb-6 sm:px-6">
        <div className="rounded-lg bg-white text-[#071638] px-5 py-6 shadow sm:px-6">
          <div className="flex flex-col w-full">
            {/* header */}
            <div className="flex md:flex-row flex-col gap-[12px] justify-between">
              <DButton
                type="button"
                onClick={() => {
                  router.back();
                }}
                className="flex items-center justify-center font-[700] text-[#000]"
              >
                <ArrowLeftIcon className="block h-6 w-6" aria-hidden="true" />
                <div>Back</div>
              </DButton>
              <DButton
                type="button"
                onClick={() => {
                  router.push('/add-contract');
                }}
                className="flex items-center justify-center shadow-md"
              >
                <PlusCircleIcon className="block h-6 w-6" aria-hidden="true" />
                <div>Add New Contract</div>
              </DButton>
            </div>
            {/* Contracts Card */}
            <div className="text-left">
              <DraftContract drafts={drafts} isDraft />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
