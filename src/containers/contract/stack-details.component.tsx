import React, { useState, useEffect, Fragment } from 'react';
import { StackDetail } from '@/components';
import { calculateValues, priceOfStack } from '@/utils';

const groupBy = (xs: any, key: any) => {
  return xs.reduce(function (rv: any, x: any) {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
};

export const StackDetails = ({ stack, setStack, margin, term, setFieldValue }: any) => {
  const [data, setData] = useState<any>({});
  const [objKeys, setObjKeys] = useState<any>([]);

  //Delete Row
  const handleDelete = (id: string) => {
    // Update View
    const updatedCategories = { ...data };
    const updatedCategoryKey = objKeys.find((key: string) => updatedCategories[key]?.some((item: any) => item.id === id));
    if (updatedCategoryKey) {
      updatedCategories[updatedCategoryKey] = updatedCategories[updatedCategoryKey]?.filter((item: any) => item.id !== id);
      setData(updatedCategories);
    }
    // Update Stack
    const newItems = stack?.stackItems?.filter((item: any) => item?.id !== id);
    const newStack = { ...stack, stackItems: newItems };
    const newStackWithPrice = { ...stack, stackItems: newItems, stackPrice: Number(priceOfStack(newStack).replace(/[^0-9.-]+/g, '')) };
    calculateValues({ margin, term, stackPrice: newStackWithPrice?.stackPrice, setFieldValue });
    setStack(newStackWithPrice);
  };

  const handleUpdate = (editedData: any) => {
    // Update Stack
    const oldItems = stack?.stackItems?.filter((item: any) => item?.id !== editedData?.id);
    const newItems = [...oldItems, editedData];
    const newStack = { ...stack, stackItems: newItems };
    const newStackWithPrice = { ...stack, stackItems: newItems, stackPrice: Number(priceOfStack(newStack).replace(/[^0-9.-]+/g, '')) };
    calculateValues({ margin, term, stackPrice: newStackWithPrice?.stackPrice, setFieldValue });
    setStack(newStackWithPrice);
  };

  useEffect(() => {
    if (stack?.stackItems && stack.stackItems.length > 0) {
      const data = groupBy(stack?.stackItems, 'category');
      setData(data);
      setObjKeys(Object.keys(data));
    } else {
      setData({});
      setObjKeys([]);
    }
  }, [stack?.stackItems]);

  return (
    <>
      {objKeys?.map((el: any) => {
        return (
          <Fragment key={el}>
            <tr className="text-[16px] font-[700] border-b border-gray-300">
              <td className="px-6 py-4">{el}</td>
            </tr>
            {data?.[el]?.map((item: any) => {
              return <StackDetail key={item?.id} onUpdate={handleUpdate} onDelete={handleDelete} {...item} />;
            })}
          </Fragment>
        );
      })}
    </>
  );
};
