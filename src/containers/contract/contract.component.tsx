import { ArrowLeftIcon } from '@heroicons/react/24/outline';
import { DButton } from '@/components';
import { Margins } from './margins.component';
import { Profits } from './profits.component';
import { ContractForm } from './contract-form.component';
import { useRouter } from 'next/router';
import { contract } from '@/schema';
import { useEffect, useState } from 'react';
import { getAll } from '@/api/stack';
import { add } from '@/api/contract';
import { priceOfStack } from '@/utils';
import { Formik, Form } from 'formik';
import { c2n } from '@/utils';

export const Contract = () => {
  const router = useRouter();
  // const [stacks, setStacks] = useState<any>([]);
  // const [selectedStack, setSelectedStack] = useState<any>();
  // useEffect(() => {
  //   (async () => {
  //     const stacks = await getAll();
  //     const newStacks = stacks?.map((stack: any) => {
  //       return {
  //         ...stack,
  //         stackPrice: Number(priceOfStack(stack).replace(/[^0-9.-]+/g, '')),
  //         // stackPrice: parseFloat(priceOfStack(stack))
  //       };
  //     });
  //     setStacks(newStacks);
  //   })();
  // }, []);

  return (
    <div className="-mt-2 border-[#CECECE]">
      <div className="mx-auto max-w-[98rem] p-6 sm:px-6 min-h-screen">
        <div className="text-[#071638] sm:px-6">
          <Formik
            initialValues={contract.initialValues}
            validationSchema={contract.schema}
            onSubmit={async (values, { setFieldValue }) => {
              const res = await add({ ...values, mrr: c2n(values?.mrr), totalContractValue: c2n(values?.totalContractValue), isDraft: true });
              router.push('/draft/' + res);
            }}
          >
            {({ errors, touched, resetForm, values, setFieldValue }) => (
              <Form className="flex md:flex-row flex-col gap-[14px]">
                {/* Form 1 */}
                <div className="flex flex-col bg-white px-5 py-6 rounded-lg shadow w-full">
                  <div className="flex md:flex-row flex-col justify-between items-center">
                    <DButton
                      type="button"
                      onClick={() => {
                        router.back();
                      }}
                      className="flex items-center justify-center font-[700] text-[#000]"
                    >
                      <ArrowLeftIcon className="block h-6 w-6" aria-hidden="true" />
                      <div>Back</div>
                    </DButton>
                    <h2 className="font-[700] uppercase text-[#000]">Add New Contract</h2>
                  </div>
                  <ContractForm
                    resetForm={resetForm}
                    values={values}
                    setFieldValue={setFieldValue}
                  // stacks={stacks}
                  // selectedStack={selectedStack}
                  // setSelectedStack={setSelectedStack}
                  />
                </div>
                {/* Form 2 */}
                {/* <div className="flex flex-col bg-white px-5 py-9 rounded-lg shadow md:w-1/2 w-full">
                  <h6 className="font-[600] text-[16px] px-[32px]">
                    Margin and Profit Calculator
                    <Margins setFieldValue={setFieldValue} values={values} selectedStack={selectedStack} />
                    <Profits values={values} />
                  </h6>
                </div> */}
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </div>
  );
};
