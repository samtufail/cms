import { Fragment } from "react";
import { ContractData } from "@/components";
import contractsData from "@/data/contract-detail.json";

export const ContractsData = () => {
  return (
    <>
      {contractsData.map((contract, index) => (
        <Fragment key={index}>
          <ContractData {...contract} />
        </Fragment>
      ))}
    </>
  );
};
