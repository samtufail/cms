import { getAll as getAllCat } from '@/api/category';
import { update } from '@/api/contract';
import { getAll } from '@/api/stack';
import { getAll as getAllItems, update as updateItem } from '@/api/stack-item';
import { CustomSelect, DButton } from '@/components';
import { calculateValues, formatCurrency, priceOfStack } from '@/utils';
import { ArrowLeftIcon, CloudIcon } from '@heroicons/react/24/outline';
import { Modal, Popconfirm, Select } from 'antd';
import { serverTimestamp } from 'firebase/firestore';
import { useRouter } from 'next/router';
import { Fragment, useEffect, useState } from 'react';
import { AddItem } from './add-item.component';
import { Discover } from './discovery-infos.component';
import { EditItem } from './edit-item.component';
import { SaveTemplate } from './save-template.component';
import { StackMargins } from './stack-details-margins.component';
import { StackProfits } from './stack-details-profits.component';

export const groupBy = (xs: any, key: any) => {
  return xs.reduce(function (rv: any, x: any) {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
};

const { Option } = Select;

const stackItemGen = ({ stackItems, categories, stack, quantities, additionalItems, deletedItems, contract }: any) => {
  const itemsWithCategory = stackItems?.map((item: any) => {
    return {
      ...item,
      categoryName: categories?.find((category: any) => category?.id === item?.category)?.name,
    };
  });
  const allStackItems = stack?.stackItems?.length ? stack?.stackItems : [];
  const allAdditionalItems = additionalItems?.length ? additionalItems : [];
  const allServices = [...allStackItems, ...allAdditionalItems];
  const finalServices = allServices?.filter((item: any) => {
    return !deletedItems?.includes(item);
  });
  const contractItems = finalServices
    ?.map((item: any) => {
      const sItem = itemsWithCategory?.find((sItem: any) => sItem?.id === item);
      const finalItem = { ...sItem, quantity: quantities?.find((quantity: any) => quantity?.id === sItem?.id)?.quantity };
      if (finalItem?.method === 'd') {
        let sumQuantity = 0;
        finalItem?.discoverySelection?.map((dItem: any) => {
          sumQuantity = sumQuantity + contract[dItem];
        });
        return { ...finalItem, quantity: sumQuantity }
      } else {
        return finalItem;
      }
    })
    .sort(function (a: any, b: any) {
      if (a?.categoryName < b?.categoryName) {
        return -1;
      }
      if (a?.categoryName > b?.categoryName) {
        return 1;
      }
      return 0;
    });
  return contractItems;
};

export const DraftDetail = ({ values, setFieldValue, contract, id }: any) => {
  const [stack, setStack] = useState<any>({});
  const [edit, setEdit] = useState(false);
  const [stackItems, setStackItems] = useState<any>([]);
  const [categories, setCategories] = useState<any>([]);
  const [open, setOpen] = useState(false);
  const [templateOpen, setTemplateOpen] = useState(false);
  const [itemOpen, setItemOpen] = useState(false);
  const [stacks, setStacks] = useState<any>([]);
  const [current, setCurrent] = useState<any>({});
  const [groupedByCat, setGroupedByCat] = useState<any>({});
  const router = useRouter();
  useEffect(() => {
    const quantities = contract?.quantities?.length ? contract?.quantities : [];
    setFieldValue('companyName', contract?.companyName);
    setFieldValue('mPrice36', contract?.mPrice36);
    setFieldValue('mProfit36', contract?.mProfit36);
    setFieldValue('mPrice24', contract?.mPrice24);
    setFieldValue('mProfit24', contract?.mProfit24);
    setFieldValue('mPrice48', contract?.mPrice48);
    setFieldValue('mProfit48', contract?.mProfit48);
    setFieldValue('mPrice60', contract?.mPrice60);
    setFieldValue('mProfit60', contract?.mProfit60);
    setFieldValue('margin', contract?.margin);
    setFieldValue('startDate', contract?.startDate);
    setFieldValue('endDate', contract?.endDate);
    setFieldValue('term', contract?.term);
    setFieldValue('locations', contract?.locations);
    setFieldValue('emailOnlyUsers', contract?.emailOnlyUsers);
    setFieldValue('workStations', contract?.workStations);
    setFieldValue('servers', contract?.servers);
    setFieldValue('computerUsers', contract?.computerUsers);
    setFieldValue('stack', contract?.stack);
    setFieldValue('isDraft', contract?.isDraft);
    setFieldValue('quantities', quantities);
    setFieldValue('additionalItems', contract?.additionalItems || []);
    setFieldValue('deletedItems', contract?.deletedItems || []);
  }, [setFieldValue, contract]);

  useEffect(() => {
    (async () => {
      const stacks = await getAll();
      const stackItems = await getAllItems();
      const categories = await getAllCat();
      const itemsWithCategory = stackItems?.map((item: any) => {
        return {
          ...item,
          categoryName: categories?.find((category: any) => category?.id === item?.category)?.name,
        };
      });
      setStackItems(itemsWithCategory);
      setCategories(categories);
      setStacks(stacks);
    })();
  }, []);

  useEffect(() => {
    const finalContract = contract || {}
    if (stacks?.length && values?.stack && stackItems?.length && categories?.length && Object?.keys(finalContract)?.length) {
      (async () => {
        const contractStack = stacks?.find((stack: any) => stack?.id === values?.stack);
        if (stackItems?.length) {
          const stacksStackItems = stackItemGen({
            stackItems,
            categories,
            stack: contractStack,
            quantities: values?.quantities,
            additionalItems: values?.additionalItems,
            deletedItems: values?.deletedItems,
            contract: finalContract,
          });
          const stack = { ...contractStack, stackItems: stacksStackItems };
          const stackPrice = Number(priceOfStack(stack).replace(/[^0-9.-]+/g, ''));
          setStack({ ...stack, stackPrice });
        }
      })();
    } else if (values?.additionalItems?.length) {
      const stacksStackItems = stackItemGen({
        stackItems,
        categories,
        stack: { stackItems: [] },
        quantities: values?.quantities,
        additionalItems: values?.additionalItems,
        deletedItems: values?.deletedItems,
        contract: finalContract,
      });
      const stack = { stackItems: stacksStackItems };
      const stackPrice = Number(priceOfStack(stack).replace(/[^0-9.-]+/g, ''));
      setStack({ ...stack, stackPrice });
    }
  }, [
    values?.stack,
    stacks,
    stackItems,
    categories,
    values?.quantities,
    values?.additionalItems,
    values?.deletedItems,
    contract,
  ]);

  useEffect(() => {
    calculateValues({ margin: values?.margin, term: values?.term, stackPrice: stack?.stackPrice, setFieldValue });
  }, [values?.margin,
  values?.term,
  stack?.stackPrice,
    setFieldValue,])


  useEffect(() => {
    if (stack?.stackItems?.length) {
      const data = groupBy(stack?.stackItems, 'categoryName');
      setGroupedByCat(data);
    }
  }, [stack])

  // Edit Service
  const editService = async ({ id, editValues }: any) => {
    const newItem = {
      name: editValues?.name,
      description: editValues?.description || '',
      unitCost: editValues?.unitCost,
      method: editValues?.method,
      discoverySelection: editValues?.discoverySelection || [],
      updatedAt: serverTimestamp(),
    };
    await updateItem(id, newItem);
    const updatedItems = await getAllItems();
    setStackItems(updatedItems);
    if (editValues?.quantity) {
      const currentQuantity = values?.quantities?.find((quantity: any) => quantity?.id === id);
      if (currentQuantity?.id) {
        const prevQuantities = values?.quantities?.filter((quantity: any) => quantity?.id !== id);
        const newQuantities = [...prevQuantities, { id, quantity: editValues?.quantity }];
        await update(contract?.id, { quantities: newQuantities });
        router.reload();
        // setFieldValue("quantities", newQuantities)
      } else {
        await update(contract?.id, { quantities: [...values?.quantities, { id, quantity: editValues?.quantity }] });
        router.reload();
        // setFieldValue("quantities", [...values?.quantities, { id, quantity: editValues?.quantity }])
      }
    }
  };

  // Delete Item
  const deleteItem = async (id: string) => {
    if (values?.deletedItems?.length) {
      const finalDI = [...values?.deletedItems, id];
      setFieldValue('deletedItems', finalDI);
    } else {
      setFieldValue('deletedItems', [`${id}`]);
    }
  };

  return (
    <div className="-mt-2 border-[#CECECE]">
      <LoadStack open={open} setOpen={setOpen} stacks={stacks} setFieldValue={setFieldValue} currentStack={values?.stack} />
      {edit && current?.name && (
        <EditItem
          editService={editService}
          setModal={setEdit}
          stackItem={current}
          setCurrent={setCurrent}
          categories={categories}
          contract={contract}
        />
      )}
      {itemOpen && (
        <AddItem
          allItems={stackItems}
          currentItems={stack?.stackItems}
          onClose={() => setItemOpen(false)}
          setFieldValue={setFieldValue}
          additionalItems={values?.additionalItems}
        />
      )}
      {templateOpen && (
        <SaveTemplate
          // allItems={stackItems}
          items={stack?.stackItems}
          onClose={() => setTemplateOpen(false)}
          id={contract?.id}
        // setFieldValue={setFieldValue}
        // additionalItems={values?.additionalItems}
        />
      )}
      <div className="mx-auto max-w-[98rem] px-4 pb-6 sm:px-6 min-h-screen">
        <div className="text-[#071638] sm:px-6">
          <div className="flex md:flex-row flex-col gap-[14px]">
            {/* Left - Header - Buttons */}
            <div className="flex flex-col bg-white px-[22px] py-6 rounded-lg shadow md:w-1/2 w-full">
              <div className="flex md:flex-row flex-col justify-between items-center">
                <DButton
                  type="button"
                  onClick={() => {
                    router.back();
                  }}
                  className="flex items-center justify-center font-[700] text-[#000]"
                >
                  <ArrowLeftIcon className="block h-6 w-6" aria-hidden="true" />
                  <div>Back</div>
                </DButton>
              </div>

              <div className="px-[14px]">
                {/* Main Heading */}
                <div className="flex flex-col text-[26px] font-[700] mt-[20px]">
                  <div className="flex items-center gap-[12px]">
                    <CloudIcon className="block h-6 w-6" aria-hidden="true" />
                    <h3>Draft Details For {contract?.companyName}</h3>
                  </div>
                  {/* <h3 className="text-[16px] font-[700] ml-[34px]">Managed Services Package</h3> */}
                </div>
                {/* Form */}
                <div className="">
                  <div className="flex justify-between mt-[24px] bg-[#F9FAFB] p-[18px] border-b border-gray-300">
                    <h6 className="text-[#6B7280] text-[14px] font-semibold">Discovery information</h6>
                    <h6 className="text-[#6B7280] text-[14px] font-semibold">QTY</h6>
                  </div>
                  <Discover values={values} setFieldValue={setFieldValue} />
                </div>
              </div>
            </div>
            {/* Right Side */}
            <div className="flex flex-col bg-white px-5 py-8 rounded-lg shadow md:w-1/2 w-full">
              <h6 className="font-[600] text-[16px] px-[32px]">
                Margin and Profit Calculator
                <StackMargins setFieldValue={setFieldValue} values={values} stack={stack} contract={contract} />
                <StackProfits setFieldValue={setFieldValue} values={values} />
              </h6>
            </div>
          </div>
          {/* Stack Table */}
          <div className="text-center p-[24px] bg-white rounded-lg my-[24px]">
            <div className="mt-4 bg-[#FFF] p-[14px] rounded-lg shadow ">
              <div className="flex items-center justify-between">
                <h3 className="text-[30px] font-[500]">Stack Details</h3>
                <div className="flex items-center gap-[8px]">
                  <DButton className="shadow-md  bg-[#4F46E5] text-[#fff] w-[140px] rounded-lg" onClick={() => setTemplateOpen(true)}>
                    Save Template
                  </DButton>
                  <DButton className="shadow-md  bg-[#4F46E5] text-[#fff] w-[140px] rounded-lg" onClick={() => setOpen(true)}>
                    Select Stack
                  </DButton>
                  <DButton className="shadow-md  bg-[#4F46E5] text-[#fff] w-[140px] rounded-lg" onClick={() => setItemOpen(true)}>
                    Add Item
                  </DButton>
                </div>
              </div>
              <div className="relative overflow-x-auto pt-[22px] shadow-md sm:rounded-lg">
                <table className="w-full text-left ">
                  <thead className="text-[12px] uppercase text-[#6B7280] border-b border-b-solid border-b-slate-200 bg-slate-50">
                    <tr>
                      <th scope="col" className="px-6 py-3">
                        Name
                      </th>
                      <th scope="col" className="px-6 py-3">
                        Description
                      </th>
                      <th scope="col" className="px-6 py-3">
                        Unit Cost
                      </th>
                      <th scope="col" className="px-6 py-3">
                        Quantity
                      </th>
                      <th scope="col" className="px-6 py-3">
                        Total Cost
                      </th>
                      <th scope="col" className="px-6 py-3">
                        Action
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {Object.keys(groupedByCat).length ? Object.keys(groupedByCat).map((category) => {
                      return (
                        <Fragment key={category}>
                          <tr className='text-[16px] font-[600] border-b border-b-solid border-b-slate-200'>
                            <td className="px-6 py-4" colSpan={6}>{category}</td>
                          </tr>
                          {groupedByCat?.[category]?.length ? groupedByCat?.[category]?.map((item: any) => {
                            const multiply = (Number(item?.quantity) * Number(item?.unitCost));
                            const unitCost = isNaN(multiply) ? '-' : `$${multiply.toFixed(2)}`;
                            return (
                              <tr className="text-[14px] font-[500] border-b border-b-solid border-b-slate-200" key={item?.id}>
                                {/* <td className="px-6 py-4">{item?.id}</td> */}
                                <td className="px-6 py-4">{item?.name}</td>
                                <td className="px-6 py-4">{item?.description ? item?.description : '-'}</td>
                                <td className="px-6 py-4">{formatCurrency(item?.unitCost)}</td>
                                <td className="px-6 py-4">{item?.quantity || "-"}</td>
                                <td className="px-6 py-4">{unitCost}</td>
                                <td className="flex px-2 py-4">
                                  <DButton
                                    type="button"
                                    onClick={() => {
                                      setCurrent(item);
                                      setEdit(true);
                                    }}
                                  >
                                    <img className="h-4 w-4" src="/icon/pencil.svg" alt="pencil" />
                                  </DButton>
                                  <Popconfirm
                                    title="Are you sure you want to delete this row?"
                                    onConfirm={() => deleteItem(item?.id)}
                                    okText="Yes"
                                    cancelText="No"
                                  >
                                    <DButton type="button">
                                      <img className="h-4 w-4" src="/icon/bin.svg" alt="bin" />
                                    </DButton>
                                  </Popconfirm>
                                </td>
                              </tr>
                            )
                          }) : null}
                        </Fragment>
                      )
                    }) : null}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className="mt-4 bg-[#FFF] p-[14px] rounded-lg shadow w-full">
            <div className="flex md:flex-row flex-col justify-end gap-[12px] p-[14px] rounded-lg shadow w-full border">
              <DButton type="button" className="shadow-md bg-[#909090] text-[#fff] w-[110px] rounded-lg">
                <div>Cancel</div>
              </DButton>
              <DButton type="submit" className="shadow-md  bg-[#4F46E5] text-[#fff] w-[140px] rounded-lg">
                <div>Save</div>
              </DButton>
              <DButton
                type="button"
                className="shadow-md  bg-[#4F46E5] text-[#fff] w-[140px] rounded-lg"
                onClick={async () => {
                  await update(id, { isDraft: false });
                  router.push('/active');
                }}
              >
                <div>Activate</div>
              </DButton>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const LoadStack = ({ open, setOpen, stacks, setFieldValue, currentStack }: any) => {
  const [id, setId] = useState('');
  return (
    <Modal
      onCancel={() => {
        setFieldValue('stack', currentStack);
        setOpen(false);
      }}
      onOk={() => {
        if (id) {
          setFieldValue('stack', id);
        }
        setOpen(false);
      }}
      open={open}
    >
      <CustomSelect
        onChange={(data: any) => {
          setId(data);
        }}
        name="custom-stack"
        label="Select Stack"
        placeholder="Select From Pre-made Stack Templates"
      >
        {stacks?.map((stack: any) => {
          return (
            <Option value={stack?.id} key={stack?.id}>
              {stack?.name}
            </Option>
          );
        })}
      </CustomSelect>
    </Modal>
  );
};
