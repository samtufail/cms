export const StackProfits = ({ values }: any) => {
  const price48 = values?.mPrice48 ? (values?.mPrice48).toFixed(2) : 0;
  const price60 = values?.mPrice60 ? (values?.mPrice60).toFixed(2) : 0;
  const price24 = values?.mPrice24 ? (values?.mPrice24).toFixed(2) : 0;
  const profit48 = values?.mProfit48 ? (values?.mProfit48).toFixed(2) : 0;
  const profit60 = values?.mProfit60 ? (values?.mProfit60).toFixed(2) : 0;
  const profit24 = values?.mProfit24 ? (values?.mProfit24).toFixed(2) : 0;

  return (
    <section className="flex flex-col gap-[15px] w-full flex-wrap mt-8 p-[20px]  border border-gray-300 rounded shadow  font-normal">
      <p className="font-[700] text-[12px] justify-end flex pt-3 border-t border-t-gray-300">Monthly Profit</p>
      <div className={`flex justify-between text-[14px] bg-[#F9FAFB] p-2`}>
        <h6 className="font-[700]">Monthly Price (48 Months)</h6>
        <p className="text-[#6B7280]">${price48}</p>
        <p className="text-[#6B7280]">${profit48}</p>
      </div>
      <div className={`flex justify-between text-[14px] p-2`}>
        <h6 className="font-[700]">Monthly Price (60 Months)</h6>
        <p className="text-[#6B7280]">${price60}</p>
        <p className="text-[#6B7280]">${profit60}</p>
      </div>
      <div className={`flex justify-between text-[14px] bg-[#F9FAFB] p-2`}>
        <h6 className="font-[700]">Monthly Price (24 Months)</h6>
        <p className="text-[#6B7280]">${price24}</p>
        <p className="text-[#6B7280]">${profit24}</p>
      </div>
    </section>
  );
};
