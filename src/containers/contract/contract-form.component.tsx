import { DButton, Input, DatePicker, CustomSelect } from '@/components';
import { calculateValues } from '@/utils';
import { Select } from 'antd';

const { Option } = Select;

const months = [{ range: 24 }, { range: 36 }, { range: 48 }, { range: 60 }];

// Auto EndDate Set
const calculateEndDate = (startDate: any, term: any) => {
  const startDateObj = new Date(startDate);
  const endDateObj = new Date(startDateObj.setMonth(startDateObj.getMonth() + parseInt(term, 10)));
  const endDate = endDateObj.toLocaleDateString('en-US', {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
  });
  return endDate;
};

export function ContractForm({ resetForm, values, setFieldValue }: any) {
  return (
    <div className="flex flex-col gap-[36px] items-center justify-center w-full mt-[22px] border-t border-t-gray-300 py-[34px]">
      <div className="flex flex-col gap-[30px] w-full justify-center text-[14px]">
        <div className="grid md:grid-cols-2 grid-cols-1 gap-[12px] md:gap-[20px] md:px-[20px] justify-center">
          <Input name="companyName" type="text" label="Company Name" placeholder="Enter Company Name" />
          {/* <CustomSelect
            onChange={(data: any) => {
              if (data) {
                const newSel = stacks?.filter((stack: any) => stack?.id === data);
                if (newSel?.length) {
                  setSelectedStack(newSel[0]);
                }
                calculateValues({ stackPrice: newSel[0]?.stackPrice, margin: values?.margin, term: values?.term, setFieldValue });
              }
            }}
            name="stack"
            type="stack"
            label="Select Stack"
            placeholder="Select From Pre-made Stacks"
          >
            {stacks?.map((stack: any) => (
              <Option key={stack?.id} value={stack?.id}>
                {stack?.name} | ${(stack?.stackPrice).toFixed(2)}
              </Option>
            ))}
          </CustomSelect> */}
          <DatePicker
            name="startDate"
            type="date"
            label="Contract Start Date"
            placeholder="MM/DD/YYYY"
            format="MM/DD/YYYY"
            onChange={(date: any) => {
              setFieldValue('startDate', date);
              if (values?.term && date) {
                const endDate = calculateEndDate(date, values.term);
                setFieldValue('endDate', endDate);
              } else {
                setFieldValue('endDate', '');
              }
            }}
          />
          <CustomSelect
            name="term"
            type="term"
            label="Contract Term (Months)"
            placeholder="24, 36, 48, 60"
            onChange={(selectedTerm: any) => {
              if (values?.startDate && selectedTerm) {
                const endDate = calculateEndDate(values?.startDate, selectedTerm);
                setFieldValue('endDate', endDate);
              } else {
                setFieldValue('endDate', '');
              }
              // calculateValues({ stackPrice: selectedStack?.stackPrice, margin: values?.margin, term: selectedTerm, setFieldValue });
            }}
          >
            {months.map((month) => (
              <Option key={month.range} value={month.range}>
                {month.range}
              </Option>
            ))}
          </CustomSelect>
          <Input name="endDate" type="text" readOnly label="Contract End Date (Auto Calculated)" placeholder="MM/DD/YYYY" />
          <Input name="locations" type="number" label="Contract Locations" placeholder="02" />
          {/* <Input name="mrr" type="text" readOnly label="MRR (Auto Calculated from Stack)" placeholder="1234" /> */}
          {/* <Input name="totalContractValue" readOnly type="text" label="Total Contract Value (Auto Calculated)" placeholder="1234" /> */}
          <Input name="computerUsers" type="number" label="Computer Users" placeholder="25" />
          <Input name="emailOnlyUsers" type="number" label="Email Only Users" placeholder="02" />
          <Input name="workStations" type="number" label="Workstations" placeholder="25" />
          <Input name="servers" type="number" label="Servers" placeholder="02" />
        </div>
        {/* Buttons */}
        <div className="flex gap-[10px] ml-auto text-[#fff] px-[22px]">
          <DButton
            className=" bg-[#909090] text-[#fff] px-[16px] rounded-lg"
            onClick={() => {
              resetForm(); // Reset the form to its initial values
            }}
          >
            Cancel
          </DButton>
          <DButton className="bg-[#4F46E5] text-[#fff] px-[16px] rounded-lg" type="submit">
            Next
          </DButton>
        </div>
      </div>
    </div>
  );
}
