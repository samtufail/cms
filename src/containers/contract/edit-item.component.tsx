import { useState } from 'react';
import { CustomSelect, DButton, Input } from '@/components';
import { message, Checkbox, Radio } from 'antd';
import type { RadioChangeEvent } from 'antd';
import { Form, Formik } from 'formik';
import { Select } from 'antd';

const { Option } = Select;

const options = [
  { label: 'Discovery', value: 'd' },
  { label: 'Custom', value: 'c' },
];

export const EditItem = ({ setModal, categories, stackItem, editService, contract }: any) => {
  const [loading, setLoading] = useState(false);

  const checkOptions = [
    { label: `Locations (${contract?.locations})`, value: 'locations' },
    { label: `Computer Users (${contract?.computerUsers})`, value: 'computerUsers' },
    { label: `Email Only Users (${contract?.emailOnlyUsers})`, value: 'emailOnlyUsers' },
    { label: `Workstations (${contract?.workStations})`, value: 'workStations' },
    { label: `Servers (${contract?.servers})`, value: 'servers' },
  ];
  return (
    <div className="fixed inset-0 z-50 flex items-center justify-center">
      <div className="fixed inset-0 bg-black opacity-50"></div>
      <div className="z-10 w-[430px] rounded-lg bg-[#fff] px-[12px] py-[15px]">
        <h2 className="mb-4 mt-[12px] text-2xl font-semibold">Edit Service / Stack Item</h2>
        <Formik
          initialValues={{
            name: stackItem?.name,
            description: stackItem?.description,
            category: stackItem?.category,
            unitCost: stackItem?.unitCost,
            quantity: stackItem?.quantity,
            method: stackItem?.method || "c",
            discoverySelection: stackItem?.discoverySelection,
          }}
          enableReinitialize
          onSubmit={async (values: any) => {}}
        >
          {({ setFieldValue, values }) => {
            return (
              <Form>
                <div className="flex flex-col gap-[6px]">
                  <Input name="name" type="text" label="Service Name" placeholder="Enter Service Name" />
                  <Input name="description" type="text" label="Description" placeholder="Enter Description" />
                  <CustomSelect
                    onChange={(data: any) => {}}
                    name="category"
                    label="Select Category"
                    placeholder="Select From Pre-made Categories"
                  >
                    {categories?.map((category: any) => (
                      <Option key={category?.id} value={category?.id}>
                        {category?.name}
                      </Option>
                    ))}
                  </CustomSelect>
                  <label htmlFor={'switch'} className="text-[14px] mb-[4px]">
                    Quantity
                  </label>
                  <Radio.Group
                    options={options}
                    onChange={({ target: { value } }: RadioChangeEvent) => {
                      setFieldValue('method', value);
                    }}
                    value={values?.method}
                    optionType="button"
                  />
                  {values?.method !== 'd' ? (
                    <Input name="quantity" type="number" label=" " placeholder="Enter Quantity" />
                  ) : (
                    <>
                      <Checkbox.Group
                        options={checkOptions}
                        defaultValue={values?.discoverySelection}
                        onChange={(values: any) => {
                          setFieldValue('discoverySelection', values);
                        }}
                      />
                    </>
                  )}
                  <Input name="unitCost" type="number" label="Unit Cost" placeholder="Enter Unit Cost" />
                </div>
                <div className="mt-4 flex justify-end gap-[4px]">
                  <DButton onClick={() => setModal(false)} className="bg-[#909090] px-[16px] text-[#fff]">
                    Cancel
                  </DButton>
                  <DButton
                    type="button"
                    onClick={async () => {
                      const isDiscovery = values?.method === 'd' && values?.discoverySelection?.length !== 0;
                      const isCustom = values?.method === 'c' && values?.quantity !== 0;
                      try {
                        if (values?.name && values?.category && values?.unitCost) {
                          setLoading(true);
                          await editService({ id: stackItem?.id, editValues: values });
                          setLoading(false);
                          setModal(false);
                        } else {
                          message.error('Please enter all the fields!');
                        }
                      } catch (e) {
                        setLoading(false);
                        setModal(false);
                      }
                    }}
                    className="w-[134px] bg-[#4F46E5] px-[12px] text-[#fff]"
                    loading={loading}
                  >
                    Save Changes
                  </DButton>
                </div>
              </Form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};
