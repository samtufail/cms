import { Input } from '@/components';
import { calculateValues } from '@/utils';

export const StackMargins = ({ setFieldValue, values, stack }: any) => {
  // Values
  const totalSeats = values?.computerUsers ? values?.computerUsers : 0;
  const price = values?.mPrice36 ? values?.mPrice36?.toFixed(2) : 0;
  const profit = values?.mProfit36 ? values?.mProfit36?.toFixed(2) : 0;
  return (
    <section className="flex flex-col gap-[12px] w-full flex-wrap mt-8 p-[12px]  border border-gray-300 rounded shadow">
      <div className={`flex justify-between text-[14px] p-2`}>
        <h6 className="font-[700]">Margin (%)</h6>
        <Input
          name="margin"
          type="number"
          label=""
          style={{ width: '150px', height: '34px' }}
          className="border border-gray-300 p-2 rounded-md w-full"
          placeholder="Enter Margin"
          customOnChange={(margin: any) => {
            calculateValues({ margin, term: values?.term, stackPrice: stack?.stackPrice, setFieldValue });
          }}
        />
      </div>
      <div className={`flex justify-between text-[14px] bg-[#F9FAFB] p-2`}>
        <h6 className="font-[700]">Monthly Price (36 Months)</h6>
        <p className="text-[#6B7280]  font-normal">${price}</p>
      </div>
      <div className={`flex justify-between text-[14px] px-2`}>
        <h6 className="font-[700]">Number of Seats</h6>
        <p className="text-[#6B7280] font-normal">{totalSeats || 0}</p>
      </div>
      <div className={`flex justify-between text-[14px] bg-[#F9FAFB] p-2`}>
        <h6 className="font-[700]">Total Per Seat</h6>
        <p className="text-[#6B7280] font-normal">
          ${typeof totalSeats === 'number' && totalSeats > 0 && values?.mPrice36 > 0 ? (values?.mPrice36 / totalSeats)?.toFixed(2) : 0}
        </p>
      </div>
      <div className={`flex justify-between text-[14px] px-2`}>
        <h6 className="font-[700]">Monthly Profit (36 Months)</h6>
        <p className="text-[#6B7280]  font-normal">${profit}</p>
      </div>
      <div className={`flex justify-between text-[14px] bg-[#F9FAFB] p-2`}>
        <h6 className="font-[700]">Contract Start Date</h6>
        <p className="text-[#6B7280]  font-normal">{values?.startDate}</p>
      </div>
      <div className={`flex justify-between text-[14px] px-2`}>
        <h6 className="font-[700]">Contract End Date</h6>
        <p className="text-[#6B7280]  font-normal">{values?.endDate}</p>
      </div>
      <div className={`flex justify-between text-[14px] bg-[#F9FAFB] p-2`}>
        <h6 className="font-[700]">Contract Term (Months)</h6>
        <p className="text-[#6B7280]  font-normal">{values?.term} month</p>
      </div>
    </section>
  );
};
