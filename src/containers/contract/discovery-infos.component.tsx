import { Discovery } from '@/components';

const dd = [
  { name: 'locations', label: 'Locations' },
  { name: 'computerUsers', label: 'Computer Users' },
  { name: 'emailOnlyUsers', label: 'Email Only Users' },
  { name: 'workStations', label: 'Worksations' },
  { name: 'servers', label: 'Servers' },

];

export const Discover = ({ values, setFieldValue }: any) => {
  return (
    <section className="flex flex-col w-full flex-wrap">
      {dd.map((info, index) => (
        <Discovery key={index} {...info} value={values?.[info?.name]} setFieldValue={setFieldValue} />
      ))}
    </section>
  );
};
