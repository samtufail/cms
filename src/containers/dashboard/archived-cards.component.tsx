import { del, update } from '@/api/contract';
import { ContractCard, DButton } from '@/components';
import { Popconfirm, Tooltip } from 'antd';
import { useRouter } from 'next/router';
import { ArchiveBoxXMarkIcon } from '@heroicons/react/24/outline';

export const ArchivedContract = ({ contracts }: any) => {
  const archivedContracts = contracts?.filter((contract: any) => contract?.isArchived);
  const router = useRouter();
  return (
    <div className="grid md:grid-cols-4 grid-cols-1 gap-2 mt-[10px]">
      {!archivedContracts?.length ? (
        <>No Archived Contracts!!</>
      ) : (
        archivedContracts?.map((contract: any, index: number) => {
          return (
            <div key={index} className="relative">
              <button key={index} className="w-full h-full">
                <ContractCard {...contract} />
              </button>
              <Popconfirm
                title="Are you sure you want to delete this contract?"
                onConfirm={async () => {
                  await del(contract?.id);
                  router.reload();
                }}
                okText="Yes"
                cancelText="No"
              >
                <DButton type="button" className="absolute top-[10px] right-[10px] p-0">
                  <Tooltip title="Delete">
                    <img className="w-4 h-4" src="/icon/bin.svg" alt="bin" />
                  </Tooltip>
                </DButton>
              </Popconfirm>
              <Popconfirm
                title="Are you sure you want to un-archive this contract?"
                onConfirm={async () => {
                  await update(contract?.id, { isArchived: false });
                  router.reload();
                }}
                okText="Yes"
                cancelText="No"
              >
                <DButton type="button" className="absolute top-[9px] right-[35px] p-0">
                  <Tooltip title="Un-Archive">
                    <ArchiveBoxXMarkIcon className="h-5 w-5 text-black" />
                  </Tooltip>
                </DButton>
              </Popconfirm>
            </div>
          );
        })
      )}
    </div>
  );
};
