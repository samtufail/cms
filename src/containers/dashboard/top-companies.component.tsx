import { Chart, DButton } from '@/components';
import { c2n } from '@/utils';
import { ArrowRightIcon } from '@heroicons/react/24/outline';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { Alerts } from './alerts.component';
import { ChartDetail } from './chart-detail.component';

export const TopCompanies = ({ contracts }: any) => {
  const router = useRouter();
  const [chartData, setChardData] = useState<any>([]);

  useEffect(() => {
    const data = contracts?.map((el: any) => {
      const mrr = typeof el?.mrr === 'string' ? c2n(el?.mrr)?.toFixed(2) : el?.mrr.toFixed(2);
      const randomColor = Math.floor(Math.random() * 16777215).toString(16);
      return { name: el?.companyName, value: Number(mrr), color: `#${randomColor}` };
    });
    console.log(data);
    setChardData(data);
  }, [contracts]);

  return (
    <div className="flex md:flex-row flex-col md:max-w-[98rem] max-w-[27rem] mx-auto pb-[34px] sm:px-6 md:gap-[34px]">
      {/* Chart Part */}
      <div className="md:w-1/2 border-[#CECECE] bg-white text-[#071638] px-5 py-6 shadow rounded-lg">
        <h2 className="text-[30px] font-[700] mb-[19px]">MRR | Top Companies</h2>
        <div>
          {chartData ? (
            <>
              <Chart chartData={chartData} />
              <ChartDetail chartData={chartData} />
            </>
          ) : (
            <></>
          )}
        </div>
        <div className="flex justify-end">
          <DButton
            type="button"
            onClick={() => {
              router.push('/finances');
            }}
            className="flex items-center justify-center font-[700] text-[#000]"
          >
            <div>View All Finances</div>
            <ArrowRightIcon className="block h-6 w-6" aria-hidden="true" />
          </DButton>
        </div>
      </div>
      {/* Content Part */}
      <div className="md:w-1/2 border-[#CECECE] bg-white text-[#071638] px-5 py-6 shadow rounded-lg">
        <h2 className="text-[30px] font-[700]">Critical Alerts</h2>
        <Alerts />
        <div className="flex justify-end">
          <DButton
            type="button"
            onClick={() => {
              router.push('/alert');
            }}
            className="flex items-center font-[700] text-[#000] "
          >
            <div className="mt-8 flex items-center gap-[3px]">
              View All Alerts
              <ArrowRightIcon className="block h-6 w-6" aria-hidden="true" />
            </div>
          </DButton>
        </div>
      </div>
    </div>
  );
};
