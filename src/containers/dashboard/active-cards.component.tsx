import { del, update } from '@/api/contract';
import { ContractCard, DButton } from '@/components';
import { Popconfirm } from 'antd';
import { useRouter } from 'next/router';
import { ArchiveBoxArrowDownIcon } from '@heroicons/react/24/outline';
import { Tooltip } from 'antd';

export const ActiveContract = ({ contracts }: any) => {
  const router = useRouter();
  return (
    <div className="grid md:grid-cols-4 grid-cols-1 gap-2 mt-[10px]">
      {contracts?.map((contract: any, index: number) => {
        if (contract?.isArchived) {
          return <></>;
        }
        return (
          <div key={index} className="relative">
            <button
              key={index}
              onClick={() => {
                router.push(`/active/${contract?.id}`);
              }}
              className="w-full h-full"
            >
              <ContractCard {...contract} />
            </button>
            <Popconfirm
              title="Are you sure you want to delete this contract?"
              onConfirm={async () => {
                await del(contract?.id);
                router.reload();
              }}
              okText="Yes"
              cancelText="No"
            >
              <DButton type="button" className="absolute top-[0px] right-[0px] p-0">
                <Tooltip title="Delete">
                  <img className="w-4 h-4" src="/icon/bin.svg" alt="bin" />
                </Tooltip>
              </DButton>
            </Popconfirm>
            <Popconfirm
              title="Are you sure you want to archive this contract?"
              onConfirm={async () => {
                await update(contract?.id, { isArchived: true });
                router.reload();
              }}
              okText="Yes"
              cancelText="No"
            >
              <DButton type="button" className="absolute top-[-2px] right-[22px] p-0">
                <Tooltip title="Archive">
                  <ArchiveBoxArrowDownIcon className="h-5 w-5 text-black" />
                </Tooltip>
              </DButton>
            </Popconfirm>
          </div>
        );
      })}
    </div>
  );
};
