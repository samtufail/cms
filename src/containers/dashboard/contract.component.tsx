import { ArrowRightIcon, PlusCircleIcon } from '@heroicons/react/24/outline';
import { DButton } from '@/components';
import { ContractCards } from './contract-cards.component';
import { useRouter } from 'next/router';

export const Contract = ({ contracts }: { contracts: any }) => {
  const router = useRouter();

  return (
    <div className="-mt-2 border-[#CECECE]">
      <div className="mx-auto max-w-[98rem] px-4 pb-6 sm:px-6">
        <div className="rounded-lg bg-white text-[#071638] px-5 py-6 shadow sm:px-6">
          <div className="flex flex-col w-full">
            {/* Header */}
            <div className="flex gap-[12px] justify-between">
              <h3 className="text-2xl sm:text-3xl font-bold">Contracts Expiring Soon</h3>
              <DButton
                type="button"
                onClick={() => {
                  router.push('/add-contract');
                }}
                className="flex items-center justify-center shadow-md"
              >
                <PlusCircleIcon className="block h-6 w-6" aria-hidden="true" />
                <span className="hidden sm:block">Add New Contract</span>
              </DButton>
            </div>

            {/* Contracts Card */}

            <ContractCards contracts={contracts} />

            {/* View Button */}
            <div>
              <DButton
                type="button"
                onClick={() => {
                  router.push('/active');
                }}
                className="flex items-center justify-center font-bold text-[#000]"
              >
                <span>View All Contracts</span>
                <ArrowRightIcon className="block h-6 w-6" aria-hidden="true" />
              </DButton>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
