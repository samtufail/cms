import { Fragment } from 'react';
import { ContractCard } from '@/components';
import contractData from '@/data/contract.json';

export const AnniversaryCards = () => {
  const displayedAnniversaryData = contractData.slice(4, 8);
  return (
    <section className="grid md:grid-cols-4 grid-cols-1 gap-[8px] w-full mt-3">
      {displayedAnniversaryData.map(({ anniversary }: any, index) => (
        <Fragment key={index}>
          <ContractCard {...anniversary} />
        </Fragment>
      ))}
    </section>
  );
};
