import { ContractCard, DButton } from '@/components';
import { Popconfirm, Tooltip } from 'antd';
import { del, update } from '@/api/contract';
import { useRouter } from 'next/router';
import { ArchiveBoxArrowDownIcon } from '@heroicons/react/24/outline';

export const ContractCards = ({ contracts }: { contracts: any }) => {
  const router = useRouter();
  return (
    <section className="grid md:grid-cols-4 grid-cols-1 gap-[4px] w-full mt-3">
      {contracts?.length ? (
        contracts.map((contract: any, index: any) => {
          return (
            <div key={index} className="relative">
              <button
                key={index}
                onClick={() => {
                  router.push(`/active/${contract?.id}`);
                }}
                className="w-full h-full"
              >
                <ContractCard {...contract} />
              </button>
              <Popconfirm
                title="Are you sure you want to delete this contract?"
                onConfirm={async () => {
                  await del(contract?.id);
                  router.reload();
                }}
                okText="Yes"
                cancelText="No"
              >
                <DButton type="button" className="absolute top-[0px] right-[0px] p-0">
                  <Tooltip title="Delete">
                    <img className="w-4 h-4" src="/icon/bin.svg" alt="bin" />
                  </Tooltip>
                </DButton>
              </Popconfirm>
              <Popconfirm
                title="Are you sure you want to archive this contract?"
                onConfirm={async () => {
                  await update(contract?.id, { isArchived: true });
                  router.push('/archived');
                }}
                okText="Yes"
                cancelText="No"
              >
                <DButton type="button" className="absolute top-[-2px] right-[22px] p-0">
                  <Tooltip title="Archive">
                    <ArchiveBoxArrowDownIcon className="h-5 w-5 text-black" />
                  </Tooltip>
                </DButton>
              </Popconfirm>
            </div>
          );
        })
      ) : (
        <>No Active Contracts!</>
      )}
    </section>
  );
};
