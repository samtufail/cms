import { ArrowRightIcon } from "@heroicons/react/24/outline";
import { DButton } from "@/components";
import { AnniversaryCards } from "./anniversary-card.component";
import { useRouter } from "next/router";

export const Anniversary = () => {
  const router = useRouter();
  return (
    <main className="border-[#CECECE]">
      <div className="mx-auto max-w-[98rem] px-4 pb-12 sm:px-6">
        <div className="rounded-lg bg-white text-[#071638] px-5 py-6 shadow sm:px-6">
          <div className="flex flex-col">
            {/* Header */}
            <div className="flex md:flex-row flex-col gap-[20px] justify-between">
              <h3 className="text-[30px] font-[700]">
                Anniversary in 2 months
              </h3>
            </div>
            {/* Contracts Card */}
            <div className="flex flex-col md:flex-row gap-[20px] w-full items-center">
              <AnniversaryCards />
            </div>
            {/* View Button */}
            <div>
              <DButton
                type="button"
                onClick={() => {
                  router.push("/active");
                }}
                className="flex items-center justify-center font-[700] text-[#000]"
              >
                <span>View All Contracts</span>
                <ArrowRightIcon className="block h-6 w-6" aria-hidden="true" />
              </DButton>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};
