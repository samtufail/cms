import { ContractCard, DButton } from '@/components';
import { Popconfirm } from 'antd';
import { del } from '@/api/contract';
import { useRouter } from 'next/router';

export const DraftContract = ({ drafts, isDraft }: any) => {
  const router = useRouter();
  // const displayedContractData = contractData.slice(0, 3);
  return (
    <div className="grid md:grid-cols-4 grid-cols-1 gap-2 mt-[10px]">
      {drafts?.map((contract: any, index: number) => {
        return (
          <div key={index} className="relative">
          <button
            onClick={() => {
              router.push(`/draft/${contract?.id}`);
            }}
            className="w-full h-full"
          >
            <ContractCard {...contract} isDraft />
          </button>
          <Popconfirm
        title="Are you sure you want to delete this draft?"
        onConfirm={async () => {
          await del(contract?.id);
          router.reload();
        }}
        okText="Yes"
        cancelText="No"
      >
        <DButton type="button" className="absolute top-0 right-0">
          <img className="w-4 h-4" src="/icon/bin.svg" alt="bin" />
        </DButton>
      </Popconfirm>
          </div>
        );
      })}
    </div>
  );
};
