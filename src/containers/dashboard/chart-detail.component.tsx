import { Fragment } from 'react';
import { ChartDetails } from '@/components';

export const ChartDetail = ({ chartData }: any) => {
  console.log(chartData);
  return (
    <section className="flex gap-[38px] w-full flex-wrap py-[40px] items-center justify-center">
      {chartData?.length ? (
        chartData?.map((contract: any, index: any) => (
          <Fragment key={index}>
            <ChartDetails {...contract} />
          </Fragment>
        ))
      ) : (
        <></>
      )}
    </section>
  );
};
