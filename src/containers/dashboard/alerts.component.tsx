import { Fragment } from "react";
import { Alert } from "@/components";
import alertData from "@/data/alert.json";

export const Alerts = () => {
  return (
    <section className="flex flex-col gap-[8px] w-full flex-wrap pt-[36px] text-[16px] font-[500]">
      {alertData.map((alert, index) => (
        <Fragment key={index}>
          <Alert {...alert} />
        </Fragment>
      ))}
    </section>
  );
};
