import { useEffect, useState } from 'react';
import { PlusCircleIcon } from '@heroicons/react/24/outline';
import { DButton } from '@/components';
import { getAll, del, update } from '@/api/stack-item';
import { getAll as getAllCategories } from '@/api/category';
import { StackItemAdd } from './stack-item-add.component';
import { Popconfirm } from 'antd';
import { StackItemEdit } from './stack-item-edit.component';
import { formatCurrency } from '@/utils';
import { serverTimestamp } from 'firebase/firestore';

export const StackItemsTable = () => {
  const [add, setAdd] = useState(false);
  const [edit, setEdit] = useState(false);
  const [stackItems, setStackItems] = useState<any>([]);
  const [categories, setCategories] = useState<any>([]);
  const [current, setCurrent] = useState<any>({});

  // Fetch All
  useEffect(() => {
    (async () => {
      const cat = await getAllCategories();
      setCategories(cat);
      const items = await getAll();
      if (items?.length) {
        const newItems = items?.map((item: any) => {
          return {
            ...item,
            categoryName: cat?.filter((category: any) => category?.id === item?.category)[0]?.name,
          }
        }).sort(function (a: any, b: any) {
          if (a?.categoryName < b?.categoryName) { return -1; }
          if (a?.categoryName > b?.categoryName) { return 1; }
          return 0;
        });
        setStackItems(newItems);
      }
    })()
  }, [])

  // Edit Service
  const editService = async ({ id, values }: any) => {
    await update(id, { ...values, description: values?.description || "", updatedAt: serverTimestamp() });
    const items = await getAll();
    if (items?.length) {
      const newItems = items?.map((item: any) => {
        return {
          ...item,
          categoryName: categories?.filter((category: any) => category?.id === item?.category)[0]?.name,
        }
      }).sort(function (a: any, b: any) {
        if (a?.categoryName < b?.categoryName) { return -1; }
        if (a?.categoryName > b?.categoryName) { return 1; }
        return 0;
      });
      setStackItems(newItems);
    }
  }

  // Delete
  const handleDelete = async (id: any) => {
    await del(id);
    const newItems = stackItems?.filter((category: any) => category?.id !== id);
    setStackItems(newItems);
  }

  return (
    <div className="-mt-2 border-[#CECECE]">
      <div className="mx-auto max-w-[98rem] px-4 pb-6 sm:px-6">
        <div className="rounded-lg bg-white text-[#071638] px-5 py-6 shadow sm:px-6">
          <div className="flex flex-col w-full">
            {/* Header */}
            <div className="flex md:flex-row flex-col gap-[12px] justify-between">
              <div></div>
              <DButton type="button"
                onClick={() => setAdd(true)}
                className="flex items-center justify-center shadow-md">
                <PlusCircleIcon className="block h-6 w-6" aria-hidden="true" />
                <div>Add New Service / Stack Item</div>
              </DButton>
            </div>
            {add && <StackItemAdd setModal={setAdd} setStackItems={setStackItems} categories={categories} />}
            {(edit && current?.name) && <StackItemEdit editService={editService} setModal={setEdit} setStackItems={setStackItems} stackItem={current} setCurrent={setCurrent} categories={categories} />}
            {/* Table  */}
            <div className="relative overflow-x-auto shadow-md sm:rounded-lg pt-[22px]">
              <table className="w-full text-left ">
                <thead className="text-[12px] text-[#6B7280] uppercase ">
                  <tr>
                    <th scope="col" className="px-6 py-3">
                      ID
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Name
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Description
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Category
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Unit Cost
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {stackItems?.map((item: any) => {
                    return <tr className="text-[14px] font-[500]" key={item?.id}>
                      <td className="px-6 py-4">{item?.id}</td>
                      <td className="px-6 py-4">{item?.name}</td>
                      <td className="px-6 py-4">{item?.description ? item?.description : "-"}</td>
                      <td className="px-6 py-4">{item?.categoryName}</td>
                      <td className="px-6 py-4">{formatCurrency(item?.unitCost)}</td>
                      <td className="flex px-2 py-4">
                        <DButton type="button"
                          onClick={() => {
                            setCurrent(item);
                            setEdit(true);
                          }}
                        >
                          <img className="w-4 h-4" src="/icon/pencil.svg" alt="pencil" />
                        </DButton>
                        <Popconfirm title="Are you sure you want to delete this row?"
                          onConfirm={() => handleDelete(item?.id)}
                          okText="Yes" cancelText="No">
                          <DButton type="button">
                            <img className="w-4 h-4" src="/icon/bin.svg" alt="bin" />
                          </DButton>
                        </Popconfirm>
                      </td>
                    </tr>
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
