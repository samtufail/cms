import { useState } from 'react';
import { CustomSelect, DButton, Input } from '@/components';
import { getAll, update } from '@/api/stack-item';
import { Checkbox, Radio, RadioChangeEvent, message } from 'antd';
import { auth } from '@/firebase.config';
import { serverTimestamp } from 'firebase/firestore';
import { Form, Formik } from 'formik';
import { Select } from 'antd';

const { Option } = Select;

const options = [
  { label: 'Discovery', value: 'd' },
  { label: 'Custom', value: 'c' },
];

const checkOptions = [
  { label: `Locations`, value: 'locations' },
  { label: `Computer Users`, value: 'computerUsers' },
  { label: `Email Only Users`, value: 'emailOnlyUsers' },
  { label: `Workstations`, value: 'workStations' },
  { label: `Servers`, value: 'servers' },
];

export const StackItemEdit = ({ setModal, categories, stackItem, editService }: any) => {
  const [loading, setLoading] = useState(false);

  const user = auth.currentUser;
  return (
    <div className="fixed inset-0 flex items-center justify-center z-50">
      <div className="fixed inset-0 bg-black opacity-50"></div>
      <div className="z-10 bg-[#fff] w-[430px] rounded-lg py-[15px] px-[12px]">
        <h2 className="text-2xl font-semibold mb-4 mt-[12px]">Edit Service / Stack Item</h2>
        <Formik
          initialValues={{
            name: stackItem?.name || "",
            description: stackItem?.description || "",
            category: stackItem?.category || "",
            unitCost: stackItem?.unitCost || 0,
            method: stackItem?.method || "c",
            discoverySelection: stackItem?.discoverySelection || [],
          }}
          enableReinitialize
          onSubmit={async (values: any) => {
            try {
              if (values?.name && values?.category && values?.unitCost) {
                setLoading(true);
                editService({ id: stackItem?.id, values });
                setLoading(false);
                setModal(false);
              } else {
                message.error('Please enter all the fields!');
              }
            } catch (e) {
              setLoading(false);
              setModal(false);
            }
          }}
        >
          {({ setFieldValue, values }) => {
            return (
              <Form>
                <div className="flex flex-col gap-[6px]">
                  <Input name="name" type="text" label="Service Name" placeholder="Enter Service Name" />
                  <Input name="description" type="text" label="Description" placeholder="Enter Description" />
                  <CustomSelect
                    onChange={(data: any) => {}}
                    name="category"
                    label="Select Category"
                    placeholder="Select From Pre-made Categories"
                  >
                    {categories?.map((category: any) => (
                      <Option key={category?.id} value={category?.id}>
                        {category?.name}
                      </Option>
                    ))}
                  </CustomSelect>
                  <Input name="unitCost" type="number" label="Unit Cost" placeholder="Enter Unit Cost" />
                  <label htmlFor={'Quantity'} className="text-[14px] mb-[4px]">
                    Quantity Calculation Method
                  </label>
                  <Radio.Group
                    options={options}
                    onChange={({ target: { value } }: RadioChangeEvent) => {
                      setFieldValue('method', value);
                    }}
                    value={values?.method}
                  />
                  {values?.method === 'c' ? (
                    <></>
                  ) : (
                    <>
                      <Checkbox.Group
                        options={checkOptions}
                        defaultValue={values?.discoverySelection}
                        onChange={(values: any) => {
                          setFieldValue('discoverySelection', values);
                        }}
                      />
                    </>
                  )}
                </div>
                <div className="mt-4 flex justify-end gap-[4px]">
                  <DButton onClick={() => setModal(false)} className="bg-[#909090] text-[#fff] px-[16px]">
                    Cancel
                  </DButton>
                  <DButton type="submit" className="bg-[#4F46E5] text-[#fff] w-[134px] px-[12px]" loading={loading}>
                    Save Changes
                  </DButton>
                </div>
              </Form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};
