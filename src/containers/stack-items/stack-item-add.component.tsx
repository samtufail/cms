import { useState } from 'react';
import { CustomSelect, DButton, Input } from '@/components';
import { add, getAll } from '@/api/stack-item';
import { Checkbox, Radio, RadioChangeEvent, message } from 'antd';
import { auth } from '@/firebase.config';
import { serverTimestamp } from 'firebase/firestore';
import { Form, Formik } from 'formik';
import { Select } from 'antd';

const { Option } = Select;

const options = [
  { label: 'Discovery', value: 'd' },
  { label: 'Custom', value: 'c' },
];

const checkOptions = [
  { label: `Locations`, value: 'locations' },
  { label: `Computer Users`, value: 'computerUsers' },
  { label: `Email Only Users`, value: 'emailOnlyUsers' },
  { label: `Workstations`, value: 'workStations' },
  { label: `Servers`, value: 'servers' },
];

export const StackItemAdd = ({ setModal, setStackItems, categories }: any) => {
  const [loading, setLoading] = useState(false);

  const user = auth.currentUser;

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50">
      <div className="fixed inset-0 bg-black opacity-50"></div>
      <div className="z-10 bg-[#fff] w-[430px] rounded-lg py-[15px] px-[12px]">
        <h2 className="text-2xl font-semibold mb-4 mt-[12px]">Add New Service / Stack Item</h2>
        <Formik initialValues={{}} onSubmit={async (values: any) => {
          try {
            if (values?.name && values?.category && values?.unitCost && values?.method) {
              setLoading(true);
              await add({ ...values, description: values?.description || "", createdBy: user?.uid, createdAt: serverTimestamp() });
              const items = await getAll();
              if (items?.length) {
                const newItems = items?.map((item: any) => {
                  return {
                    ...item,
                    categoryName: categories?.filter((category: any) => category?.id === item?.category)[0]?.name,
                  }
                }).sort(function (a: any, b: any) {
                  if (a?.categoryName < b?.categoryName) { return -1; }
                  if (a?.categoryName > b?.categoryName) { return 1; }
                  return 0;
                });
                setStackItems(newItems);
              }
              setModal(false);
            } else {
              message.error("Please enter all the fields!")
            }
          } catch (e) {
            console.log(e)
            setModal(false);
          }
        }}>
          {({ setFieldValue, values }) => {
            return (
              <Form>
                <div className="flex flex-col gap-[6px]">
                  <Input name="name" type="text" label="Service Name" placeholder="Enter Service Name" />
                  <Input name="description" type="text" label="Description" placeholder="Enter Description" />
                  <CustomSelect
                    onChange={(data: any) => {
                    }}
                    name="category"
                    label="Select Category"
                    placeholder="Select From Pre-made Categories"
                  >
                    {categories?.map((category: any) => (
                      <Option key={category?.id} value={category?.id}>
                        {category?.name}
                      </Option>
                    ))}
                  </CustomSelect>
                  <Input name="unitCost" type="number" label="Unit Cost" placeholder="Enter Unit Cost" />
                  <label htmlFor={'Quantity'} className="text-[14px] mb-[4px]">
                    Quantity Calculation Method
                  </label>
                  <Radio.Group
                    options={options}
                    onChange={({ target: { value } }: RadioChangeEvent) => {
                      setFieldValue('method', value);
                    }}
                    value={values?.method}
                  />
                  {values?.method !== 'd' ? (
                    <></>
                  ) : (
                    <>
                      <Checkbox.Group
                        options={checkOptions}
                        defaultValue={['']}
                        onChange={(values: any) => {
                          setFieldValue('discoverySelection', values);
                        }}
                      />
                    </>
                  )}
                </div>
                <div className="mt-4 flex justify-end gap-[4px]">
                  <DButton onClick={() => setModal(false)} className="bg-[#909090] text-[#fff] px-[16px]">
                    Cancel
                  </DButton>
                  <DButton type="submit" className="bg-[#4F46E5] text-[#fff] w-[134px] px-[12px]" loading={loading}>
                    Save Changes
                  </DButton>
                </div>
              </Form>
            )
          }}
        </Formik>
      </div>
    </div>
  );
};
