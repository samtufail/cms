import { useState } from 'react';
import { DButton } from '@/components';
import { add, getAll } from '@/api/category';
import { message } from 'antd';
import { auth } from '@/firebase.config';
import { serverTimestamp } from 'firebase/firestore';

export const CategoryAdd = ({ setModal, setCategories }: any) => {
  const [categoryName, setCategoryName] = useState('');
  const [loading, setLoading] = useState(false);

  const user = auth.currentUser;

  const addCategory = async () => {
    try {
      if (categoryName) {
        setLoading(true);
        await add({ name: categoryName, createdBy: user?.uid, createdAt: serverTimestamp() });
        const cat = await getAll();
        setCategories(cat);
        setModal(false);
      } else {
        message.error("Category name cannot be empty!")
      }
    } catch (e) {
      console.log(e)
      setModal(false);
    }
  }

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50">
      <div className="fixed inset-0 bg-black opacity-50"></div>
      <div className="z-10 bg-[#fff] w-[430px] rounded-lg py-[15px] px-[12px]">
        <h2 className="text-2xl font-semibold mb-4 mt-[12px]">Add New Category</h2>
        <div className="flex flex-col gap-[6px]">
          <label>Category Name</label>
          <input
            type="text"
            title="Service Name"
            className="border border-gray-300 p-2 rounded-md w-full"
            value={categoryName}
            onChange={(e) => setCategoryName(e.target.value)}
          />
        </div>
        <div className="mt-4 flex justify-end gap-[4px]">
          <DButton onClick={() => setModal(false)} className="bg-[#909090] text-[#fff] px-[16px]">
            Cancel
          </DButton>
          <DButton onClick={addCategory} className="bg-[#4F46E5] text-[#fff] w-[134px] px-[12px]" loading={loading}>
            Save Changes
          </DButton>
        </div>
      </div>
    </div>
  );
};
