import { useEffect, useState } from 'react';
import { PlusCircleIcon } from '@heroicons/react/24/outline';
import { DButton } from '@/components';
import { getAll, del } from '@/api/category';
import { CategoryAdd } from './category-add.component';
import { Popconfirm } from 'antd';
import { CategoryEdit } from './category-edit.component';

export const CategoriesTable = () => {
  const [add, setAdd] = useState(false);
  const [edit, setEdit] = useState(false);
  const [categories, setCategories] = useState<any>([]);
  const [current, setCurrent] = useState<any>({});

  // Fetch All
  useEffect(() => {
    (async () => {
      const cat = await getAll()
      setCategories(cat);
    })()
  }, [])

  // Delete
  const handleDelete = async (id: any) => {
    await del(id);
    const newCat = categories?.filter((category: any) => category?.id !== id);
    setCategories(newCat);
  }

  return (
    <div className="-mt-2 border-[#CECECE]">
      <div className="mx-auto max-w-[98rem] px-4 pb-6 sm:px-6">
        <div className="rounded-lg bg-white text-[#071638] px-5 py-6 shadow sm:px-6">
          <div className="flex flex-col w-full">
            {/* Header */}
            <div className="flex md:flex-row flex-col gap-[12px] justify-between">
              <div></div>
              <DButton type="button"
                onClick={() => setAdd(true)}
                className="flex items-center justify-center shadow-md">
                <PlusCircleIcon className="block h-6 w-6" aria-hidden="true" />
                <div>Add New Category</div>
              </DButton>
            </div>
            {add && <CategoryAdd setModal={setAdd} setCategories={setCategories} />}
            {(edit && current?.name) && <CategoryEdit setModal={setEdit} setCategories={setCategories} category={current} setCurrent={setCurrent} />}
            {/* Table  */}
            <div className="relative overflow-x-auto shadow-md sm:rounded-lg pt-[22px]">
              <table className="w-full text-left ">
                <thead className="text-[12px] text-[#6B7280] uppercase ">
                  <tr>
                    <th scope="col" className="px-6 py-3 w-[400px]">
                      ID
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Category Name
                    </th>
                    <th scope="col" className="px-6 py-3 w-[300px]">
                      Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {categories?.map((category: any) => {
                    return <tr className="text-[14px] font-[500]" key={category?.id}>
                      <td className="px-6 py-4">{category?.id}</td>
                      <td className="px-6 py-4">{category?.name}</td>
                      <td className="flex px-2 py-4">
                        <DButton type="button"
                          onClick={() => {
                            setCurrent(category);
                            setEdit(true);
                          }}
                        >
                          <img className="w-4 h-4" src="/icon/pencil.svg" alt="pencil" />
                        </DButton>
                        <Popconfirm title="Are you sure you want to delete this row?"
                          onConfirm={() => handleDelete(category?.id)}
                          okText="Yes" cancelText="No">
                          <DButton type="button">
                            <img className="w-4 h-4" src="/icon/bin.svg" alt="bin" />
                          </DButton>
                        </Popconfirm>
                      </td>
                    </tr>
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
