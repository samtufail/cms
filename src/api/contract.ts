import { db } from '@/firebase.config';

import { addDoc, collection, deleteDoc, doc, getDoc, getDocs, query, updateDoc } from 'firebase/firestore';

const collectionName = 'contract';

// Get All
export const getAll = async () => {
  const data: any = [];
  const q = query(collection(db, collectionName));

  const querySnapshot = await getDocs(q);

  querySnapshot.forEach(async (document) => {
    data?.push({
      id: document.id,
      ...document?.data(),
    });
  });

  return data;
};

// Get by ID
export const getById = async (id: string): Promise<any> => {
  const docRef = doc(db, collectionName, id);
  const docSnap = await getDoc(docRef);
  return {
    id: docSnap.id,
    ...docSnap?.data(),
  };
};

// Add
export const add = async (data: any) => {
  const docRef = await addDoc(collection(db, collectionName), data);
  return docRef?.id;
};

// Update
export const update = async (id: string, data: any) => {
  const ref = doc(db, collectionName, id);
  await updateDoc(ref, { ...data });
};

// Delete
export const del = async (id: string) => {
  await deleteDoc(doc(db, collectionName, id));
};
