export async function asyncForEach(array: any, callback: any) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

// Format a number to currency
export const formatCurrency = (value: number): string => {
  let USDollar = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  });
  return USDollar.format(value);
};

// Format a currency to number
export const c2n = (value: string): number => {
  return Number(value.replace(/[^0-9.-]+/g, ''));
};

// Get Time of the Day
export function timeOfDay() {
  let hour = new Date().getHours();
  if (hour >= 4 && hour <= 11) return 'Morning';
  if (hour >= 12 && hour <= 16) return 'Afternoon';
  if (hour >= 17 && hour <= 20) return 'Evening';
  if (hour >= 21 || hour <= 3) return 'Night';
}

// Get Price for Stack
export const priceOfStack = (stack: any = []) => {
  let price = 0;
  stack?.stackItems?.forEach((item: any) => {
    if (item?.quantity) {
      price += item?.unitCost * item?.quantity;
    }
  });
  return formatCurrency(price);
};

// Calculate Values
export const calculateValues = ({ stackPrice, margin, term, setFieldValue }: any) => {
  if (stackPrice && margin) {
    // Monthly Price
    const marginP = margin / 100;
    const mPrice36 = stackPrice / (1 - marginP);
    const mPrice24 = mPrice36 + mPrice36 * 0.05;
    const mPrice48 = mPrice36 - mPrice36 * 0.05;
    const mPrice60 = mPrice36 - mPrice36 * 0.1;

    // Monthly Profit
    const mprofit36 = mPrice36 - stackPrice;
    const mprofit24 = mPrice24 - stackPrice;
    const mprofit48 = mPrice48 - stackPrice;
    const mprofit60 = mPrice60 - stackPrice;

    // TVC
    if (term) {
      let totalContractValue;
      let mrr;
      switch (term) {
        case 24:
          totalContractValue = mPrice24 * term;
          mrr = mPrice24;
          break;
        case 48:
          totalContractValue = mPrice48 * term;
          mrr = mPrice48;
          break;
        case 36:
          totalContractValue = mPrice36 * term;
          mrr = mPrice36;
          break;
        case 60:
          totalContractValue = mPrice60 * term;
          mrr = mPrice60;
          break;
      }
      setFieldValue('totalContractValue', `$${totalContractValue?.toFixed(2)}`);
      setFieldValue('mrr', `$${mrr?.toFixed(2)}`);
    } else {
      setFieldValue('totalContractValue', '');
      setFieldValue('mrr', '');
    }

    //set monthly price
    setFieldValue('mPrice36', mPrice36);
    setFieldValue('mPrice24', mPrice24);
    setFieldValue('mPrice48', mPrice48);
    setFieldValue('mPrice60', mPrice60);

    //set monthly profit
    setFieldValue('mProfit36', mprofit36);
    setFieldValue('mProfit24', mprofit24);
    setFieldValue('mProfit48', mprofit48);
    setFieldValue('mProfit60', mprofit60);
  } else {
    setFieldValue('monthlyPrice', '');
    setFieldValue('monthlyProfit', '');
        //set monthly price
        setFieldValue('mPrice36', '');
        setFieldValue('mPrice24', '');
        setFieldValue('mPrice48', '');
        setFieldValue('mPrice60', '');

        //set monthly profit
        setFieldValue('mProfit36', '');
        setFieldValue('mProfit24', '');
        setFieldValue('mProfit48', '');
        setFieldValue('mProfit60', '');
  }
};
