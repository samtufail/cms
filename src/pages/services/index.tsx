import { Header, Layout } from "@/components";
import { StackItemsTable } from "@/containers/stack-items";

export default function Categories() {
  return (
    <Layout>
      <Header />
      <StackItemsTable />
    </Layout>
  )
}