import { Button } from "@/components";
import { useRouter } from "next/router";

export default function EmailSentPage() {
  const router = useRouter();
  return (
    <div className="bg-[#071638] text-[#fff] grid md:grid-cols-2 grid-cols-1 min-h-screen">
      {/* Left-side Content*/}
      <div
        className="flex flex-col gap-[36px] items-center justify-center"
        style={{ borderRight: "0.5px solid #1B2431" }}
      >
        <div className="px-[16px]">
          <img src="/logo.png" alt="logo" />
        </div>
        <p className="hidden md:flex text-[16px] font-[400] text-[#64748B] max-w-[318px] text-center">
          Manage, track, and receive alerts on your client contracts seamlessly.
        </p>
        <div>
          <img
            src="/login/side-image.png"
            alt="side-image"
            className="hidden md:flex max-w-[349px]"
          />
        </div>
      </div>
      {/* Right-side Content*/}
      <div className="flex flex-col gap-[36px] items-center justify-center">
        <div className="flex flex-col gap-[18px] w-full items-center max-w-[428px] p-[12px]">
          <img src="/icon/check.svg" alt="check-icon" />
          <h1 className="md:text-[33px] text-[26px]">Email Sent</h1>
          <p className="max-w-[385px] md:text-[18px] text-[16px] text-center">
            We&apos;ve sent a password reset link to your email. Please check
            your inbox (and spam folder) and follow the instructions.
          </p>
          <Button type="button" onClick={() => router.push("/")}>
            Go Back to Login
          </Button>
        </div>
      </div>
    </div>
  );
}
