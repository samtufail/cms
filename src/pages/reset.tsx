import { Formik, Form } from "formik";
import { resetPass } from "@/schema";
import { Button, Input } from "@/components";
import { useRouter } from "next/router";

export default function Reset() {
  const router = useRouter();

  return (
    <div className="bg-[#071638] text-[#fff] grid md:grid-cols-2 grid-cols-1 min-h-screen">
      {/* Left-side Content*/}
      <div
        className="flex flex-col gap-[36px] items-center justify-center"
        style={{ borderRight: "0.5px solid #1B2431" }}
      >
        <div className="px-[12px]">
          <img src="/logo.png" alt="logo" />
        </div>
        <p className="hidden md:flex max-w-[318px] text-center text-[16px] font-[400] text-[#64748B]">
          Mange, track, and receive alerts on your client contracts seamlessly.
        </p>
        <div>
          <img
            src="/login/side-image.png"
            alt="side-image"
            className="hidden md:flex max-w-[349px]"
          />
        </div>
      </div>
      {/* Right-side Content*/}
      <div className="flex flex-col gap-[36px] items-center justify-center">
        <h1 className="md:text-[33px] text-[28px]">Reset Password</h1>
        <Formik
          initialValues={resetPass.initialValues}
          validationSchema={resetPass.schema}
          onSubmit={(values) => {
            // same shape as initial values
            console.log(values);
            router.push("/");
          }}
        >
          {({ errors, touched }) => (
            <Form className="flex flex-col gap-[30px] w-full max-w-[428px] p-[12px]">
              <div className="flex flex-col gap-[6px]">
                <Input
                  name="password"
                  type="password"
                  label="Password"
                  placeholder="••••••••••••"
                />
                <Input
                  name="confirmPass"
                  type="password"
                  label="Confirm Password"
                  placeholder="••••••••••••"
                />
              </div>
              <Button type="submit">Reset Password</Button>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}
