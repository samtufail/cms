import { Header, Layout } from "@/components";
import { Finance } from "@/containers/finance";

export default function FinancePage() {
  return (
    <div>
      <Layout>
        <Header />
        <Finance />
      </Layout>
    </div>
  );
}
