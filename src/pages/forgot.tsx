import { Formik, Form } from 'formik';
import { reset } from '@/schema';
import { Button, Input } from '@/components';
import { useRouter } from 'next/router';
import { auth } from '@/firebase.config';
import { sendPasswordResetEmail } from 'firebase/auth';

export default function Forget() {
  const router = useRouter();
  return (
    <div className="bg-[#071638] text-[#fff] grid md:grid-cols-2 grid-cols-1 min-h-screen">
      {/* Left-side Content*/}
      <div className="flex flex-col gap-[36px] items-center justify-center" style={{ borderRight: '0.5px solid #1B2431' }}>
        <div className="px-[16px]">
          <img src="/logo.png" alt="logo" />
        </div>
        <p className="hidden md:flex text-[16px] font-[400] text-[#64748B] max-w-[318px] text-center">
          Manage, track, and receive alerts on your client contracts seamlessly.
        </p>
        <div>
          <img src="/login/side-image.png" alt="side-image" className="hidden md:flex max-w-[349px]" />
        </div>
      </div>
      {/* Right-side Content*/}
      <div className="flex flex-col gap-[36px] items-center justify-center px-[8px]">
        <div className="flex flex-col gap-[24px] justify-center items-center text-center">
          <h1 className="md:text-[33px] text-[26px]">Forgot Password?</h1>
          <p className="max-w-[350px] text-[18px] text-center">
            If your email exist in our system, we will send you a link to reset your password.
          </p>
        </div>
        <Formik
          initialValues={reset.initialValues}
          validationSchema={reset.schema}
          onSubmit={async (values) => {
            try {
              const res = await sendPasswordResetEmail(auth, values?.email);
              console.log(res);
              router.push('/email-sent');
            } catch (err) {
              console.log(err);
            }
          }}
        >
          {({ errors, touched }) => (
            <Form className="flex flex-col gap-[30px] w-full max-w-[428px] p-[12px]">
              <div className="flex flex-col gap-[6px]">
                <Input name="email" type="email" label="Email" placeholder="you@example.com" />
              </div>
              <Button type="submit">Reset Password</Button>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}
