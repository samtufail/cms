import { getAll } from '@/api/contract';
import { Header, Layout } from '@/components';
import { Contract, Anniversary, TopCompanies } from '@/containers/dashboard';
import { useEffect, useState } from 'react';

export default function Dashboard() {
  // Fetch Contracts and Set in the State
  const [contracts, setContracts] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    (async () => {
      setLoading(true);
      const contracts = await getAll();
      const active = contracts?.filter((contract: any) => !contract?.isDraft);
      setContracts(active);
      setLoading(false);
    })();
  }, []);

  return (
    <div>
      <Layout loading={loading}>
        {!loading ? (
          <>
            <Header />
            <Contract contracts={contracts} />
            {/* TODO: Add Anniversary in 2 months data */}
            {/* <Anniversary /> */}
            <TopCompanies contracts={contracts} />
          </>
        ) : (
          <></>
        )}
      </Layout>
    </div>
  );
}
