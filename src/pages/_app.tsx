import { auth } from '@/firebase.config';
import { logout, signIn, wrapper } from '@/redux';
import '@/styles/globals.css';
import { ConfigProvider } from 'antd';
import { onAuthStateChanged } from 'firebase/auth';
import type { AppProps } from 'next/app';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { theme } from '../theme/themeConfig';

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();
  const dispatch = useDispatch();
  // check at page load if a user is authenticated
  useEffect(() => {
    onAuthStateChanged(auth, (userAuth) => {
      if (userAuth) {
        // user is logged in, send the user's details to redux, store the current user in the state
        dispatch(
          signIn({
            email: userAuth.email,
            uid: userAuth.uid,
            displayName: userAuth.displayName,
            photoUrl: userAuth.photoURL,
          })
        );
        if (router.pathname === '/') {
          router.push('/dashboard');
        }
      } else {
        dispatch(logout());
        if (router.pathname === '/dashboard') {
          router.push('/');
        }
      }
    });
  }, [router, dispatch]);

  return (
    <ConfigProvider theme={theme}>
      <Component {...pageProps} />
    </ConfigProvider>
  );
}

export default wrapper.withRedux(MyApp);
