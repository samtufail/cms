import { Header, Layout } from '@/components';
import { getAll } from '@/api/contract';
import { useEffect, useState } from 'react';
import { Archive } from '@/containers/contract/archive.component';

export default function ActiveContracts() {
  // Fetch contracts and Set in the State
  const [contracts, setcontracts] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    (async () => {
      setLoading(true);
      const contracts = await getAll();
      const drafts = contracts?.filter((contract: any) => !contract?.isDraft);
      setcontracts(drafts);
      setLoading(false);
    })();
  }, []);

  return (
    <div>
      <Layout loading={loading}>
        {!loading ? (
          <>
            <Header />
            <Archive contracts={contracts} />
          </>
        ) : (
          <></>
        )}
      </Layout>
    </div>
  );
}
