import { Header, Layout } from "@/components";
import { CategoriesTable } from "@/containers/categories";

export default function Categories() {
  return (
    <Layout>
      <Header />
      <CategoriesTable />
    </Layout>
  )
}