import { Formik, Form } from 'formik';
import { login } from '@/schema';
import { Button, Input } from '@/components';
import { useRouter } from 'next/router';
import { auth, doSignInWithPopup } from '@/firebase.config';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { useDispatch } from 'react-redux';
import { signIn } from '@/redux';
import { useState } from 'react';
import BarLoader from 'react-spinners/BarLoader';

export default function LoginPage() {
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const dispatch = useDispatch();

  return (
    <>
      <div className="bg-[#071638] text-[#fff] grid md:grid-cols-2 grid-cols-1 min-h-screen">
        {/* Logo */}
        <div className="flex flex-col gap-[36px] items-center justify-center" style={{ borderRight: '0.5px solid #1B2431' }}>
          <div className="px-[12px]">
            <img src="/logo.png" alt="logo" />
          </div>
          <p className="hidden md:flex text-[16px] font-[400] text-[#64748B] max-w-[318px] text-center">
            Manage, track, and receive alerts on your client contracts seamlessly.
          </p>
          <div>
            <img src="/login/side-image.png" alt="side-image" className="hidden md:flex max-w-[349px]" />
          </div>
        </div>
        {/* Inputs */}
        <div className="flex flex-col gap-[36px] items-center justify-center">
          <h1 className="md:text-[33px] text-[22px] text-center">Contract Management System</h1>
          {error ? <p className="text-red-600">{error}</p> : <></>}
          <Formik
            initialValues={login.initialValues}
            validationSchema={login.schema}
            onSubmit={(values) => {
              setLoading(true);
              // Sign in an existing user with Firebase
              signInWithEmailAndPassword(auth, values?.email, values?.password)
                // returns  an auth object after a successful authentication
                // userAuth.user contains all our user details
                .then((userAuth) => {
                  // store the user's information in the redux state
                  dispatch(
                    signIn({
                      email: userAuth.user.email,
                      uid: userAuth.user.uid,
                      displayName: userAuth.user.displayName,
                      photoUrl: userAuth.user.photoURL,
                    })
                  );
                  router.push('/dashboard');
                  setLoading(false);
                })
                // display the error if any
                .catch((err) => {
                  setError(err?.code);
                  setLoading(false);
                });
            }}
          >
            {({ errors, touched }) => (
              <Form className=" flex flex-col gap-[30px] w-full max-w-[428px] p-[12px]">
                <div className="flex flex-col gap-[6px]">
                  <Input name="email" type="email" label="Email" placeholder="you@example.com" />
                  <Input
                    name="password"
                    type="password"
                    label="Password"
                    placeholder="••••••••••••"
                    customErrorLabel={{
                      text: 'Forgot Password',
                      action: () => router.push('/forgot'),
                    }}
                    alwaysShowErrorLabel
                  />
                </div>
                <Button type="submit" disabled={loading} className="flex justify-center items-center">
                  {loading ? <BarLoader loading={true} aria-label="Loading Spinner" data-testid="loader" color="#1e3b7d" /> : 'Sign In'}
                </Button>
                <Button
                  type="button"
                  onClick={async () => {
                    try {
                      const res = await doSignInWithPopup();
                      dispatch(
                        signIn({
                          email: res.user.email,
                          uid: res.user.uid,
                          displayName: res.user.displayName,
                          photoUrl: res.user.photoURL,
                        })
                      );
                    } catch (e: any) {
                      console.error(e?.message);
                    }
                  }}
                  className="flex gap-[14px] items-center justify-center"
                >
                  <img src="/login/microsoft.png" alt="microsoft" className="w-[20px]" />
                  <div>Sign in with Microsoft</div>
                </Button>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </>
  );
}
