import { Header, Layout } from "@/components";
import { Alert } from "@/containers/alert";

export default function AlertPage() {
  return (
    <div>
      <Layout>
        <Header />
        <Alert />
      </Layout>
    </div>
  );
}
