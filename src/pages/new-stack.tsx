import { Header, Layout } from "@/components";
import { ManageStack } from "@/containers/stack";

export default function NewStack() {
  return (
    <div>
      <Layout>
        <Header />
        <ManageStack />
      </Layout>
    </div>
  );
}
