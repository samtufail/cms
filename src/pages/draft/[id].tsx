import { useState, useEffect } from 'react';
import { Header, Layout } from '@/components';
import { DraftDetail } from '@/containers/contract';
import { Form, Formik } from 'formik';
import { getById, update } from '@/api/contract';
import { getAll } from '@/api/stack';
import { useRouter } from 'next/router';
import { priceOfStack, c2n } from '@/utils';

const iValues: any = {};

export default function ActiveContract() {
  const [contract, setContract] = useState<any>();
  const [loading, setLoading] = useState(false);

  const router = useRouter();

  const id = router?.query?.id as string;

  useEffect(() => {
    (async () => {
      if (id) {
        setLoading(true);
        const contract = await getById(id);
        setContract(contract);
        setLoading(false);
      }
    })();
  }, [id]);
  return (
    <div>
      <Layout loading={loading}>
        <Header />
        <Formik
          initialValues={iValues}
          onSubmit={async (values) => {
            await update(id, { ...values, mrr: values?.mrr ? c2n(values?.mrr) : 0, totalContractValue: values?.totalContractValue ? c2n(values?.totalContractValue) : 0 });
            router.reload();
          }}
        >
          {({ values, setFieldValue }) => (
            <Form>
              <DraftDetail values={values} setFieldValue={setFieldValue} contract={contract} id={id} />
            </Form>
          )}
        </Formik>
      </Layout>
    </div>
  );
}
