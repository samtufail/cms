import { Inter } from 'next/font/google';
import Head from 'next/head';
import LoginPage from './loginPage';

const inter = Inter({ subsets: ['latin'] });

export default function Home() {
  return (
    <>
      <Head>
        <title>Contract Management System</title>
      </Head>
      <main className={`${inter.className}`}>
        <LoginPage />
      </main>
    </>
  );
}
