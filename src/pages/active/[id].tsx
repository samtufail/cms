import { getById, update } from '@/api/contract';
import { getById as getStackById, update as updateStack } from '@/api/stack';
import { Header, Layout } from '@/components';
import { ContractDetail } from '@/containers/contract';
import { storage } from '@/firebase.config';
import { priceOfStack } from '@/utils';
import { getDownloadURL, ref, uploadBytes } from 'firebase/storage';
import { Form, Formik } from 'formik';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { v4 } from 'uuid';

export default function ActiveContract() {
  const [stack, setStack] = useState<any>();
  const [contract, setContract] = useState<any>();
  const [loading, setLoading] = useState(false);
  const [pdf, setPDF] = useState<any>(null);

  const router = useRouter();

  const id = router?.query?.id as string;

  useEffect(() => {
    (async () => {
      if (id) {
        setLoading(true);
        const contract = await getById(id);
        setContract(contract);
        const stack = await getStackById(contract?.stack);
        setStack({ ...stack, stackPrice: Number(priceOfStack(stack).replace(/[^0-9.-]+/g, '')) });
        setLoading(false);
      }
    })();
  }, [id]);

  return (
    <div>
      <Layout loading={loading}>
        <Header />
        <Formik
          initialValues={{ locations: 0, computerUsers: 0 }}
          onSubmit={async (values) => {
            await update(id, { ...values });
            await updateStack(stack?.id, { id: stack?.id, name: stack?.name, stackItems: stack?.stackItems });
            if (pdf !== null) {
              const pdfRef = ref(storage, `contracts/${contract?.companyName}-${v4()}`);
              const res = await uploadBytes(pdfRef, pdf);
              const url = await getDownloadURL(res?.ref);
              await update(id, { 'contract-pdf': url });
            }
            router.reload();
          }}
        >
          {({ values, setFieldValue }) => (
            <Form>
              <ContractDetail values={values} setFieldValue={setFieldValue} stack={stack} setStack={setStack} contract={contract} setPDF={setPDF} pdf={pdf} id={id} />
            </Form>
          )}
        </Formik>
      </Layout>
    </div>
  );
}
