import { Header, Layout } from '@/components';
import { Active } from '@/containers/contract';
import { getAll } from '@/api/contract';
import { useEffect, useState } from 'react';

export default function ActiveContracts() {
  // Fetch contracts and Set in the State
  const [contracts, setcontracts] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    (async () => {
      setLoading(true);
      const contracts = await getAll();
      const drafts = contracts?.filter((contract: any) => !contract?.isDraft);
      setcontracts(drafts);
      setLoading(false);
    })();
  }, []);

  return (
    <div>
      <Layout loading={loading}>
        {!loading ? (
          <>
            <Header />
            <Active contracts={contracts} />
          </>
        ) : (
          <></>
        )}
      </Layout>
    </div>
  );
}
