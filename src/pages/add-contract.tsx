import { Header, Layout } from '@/components';
import { Contract } from '@/containers/contract';

export default function ActiveContract() {
  return (
    <div>
      <Layout>
        <Header />
        <Contract />
      </Layout>
    </div>
  );
}
