import { getAll } from '@/api/stack';
import { getAll as getAllItems } from '@/api/stack-item';
import {getAll as getAllContracts} from '@/api/contract';
import { Header, Layout } from '@/components';
import { Stacks } from '@/containers/stack';
import { useEffect, useState } from 'react';

export default function Stack() {
  // Fetch Stacks and Set in the State
  const [stacks, setStacks] = useState([]);
  const [contracts, setContracts] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    (async () => {
      setLoading(true);
      const stacks = await getAll();
      const stackItems = await getAllItems();
      const finalStacks = stacks?.map((stack: any) => {
        const items = stack?.stackItems?.filter((item: any) => {
          const itemDetails = stackItems?.find((sItem: any) => sItem?.id === item);
          if (itemDetails?.id) {
            return true
          }
        })
        return { ...stack, stackItems: !items?.length ? [] : items }
      })
      const contracts = await getAllContracts();
      setContracts(contracts);
      setStacks(finalStacks);
      setLoading(false);
    })();
  }, []);

  return (
    <div>
      <Layout loading={loading}>
        {!loading ? (
          <>
            <Header />
            <Stacks stacks={stacks} contracts={contracts} />
          </>
        ) : (
          <></>
        )}
      </Layout>
    </div>
  );
}
